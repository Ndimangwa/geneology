-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: localhost    Database: dbgeneology
-- ------------------------------------------------------
-- Server version	8.0.27-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `_approvalSequenceData`
--

DROP TABLE IF EXISTS `_approvalSequenceData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_approvalSequenceData` (
  `dataId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `schemaId` int unsigned NOT NULL,
  `requestedBy` int unsigned NOT NULL,
  `nextJobToApprove` int unsigned DEFAULT NULL,
  `alreadyApprovedList` varchar(255) DEFAULT NULL,
  `timeOfRegistration` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `specialInstruction` varchar(32) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`dataId`),
  KEY `schemaId` (`schemaId`),
  KEY `requestedBy` (`requestedBy`),
  KEY `nextJobToApprove` (`nextJobToApprove`),
  CONSTRAINT `_approvalSequenceData_ibfk_1` FOREIGN KEY (`schemaId`) REFERENCES `_approvalSequenceSchema` (`schemaId`),
  CONSTRAINT `_approvalSequenceData_ibfk_2` FOREIGN KEY (`requestedBy`) REFERENCES `_login` (`loginId`),
  CONSTRAINT `_approvalSequenceData_ibfk_3` FOREIGN KEY (`nextJobToApprove`) REFERENCES `_jobTitle` (`jobId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_approvalSequenceData`
--

LOCK TABLES `_approvalSequenceData` WRITE;
/*!40000 ALTER TABLE `_approvalSequenceData` DISABLE KEYS */;
/*!40000 ALTER TABLE `_approvalSequenceData` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_approvalSequenceSchema`
--

DROP TABLE IF EXISTS `_approvalSequenceSchema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_approvalSequenceSchema` (
  `schemaId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `schemaName` varchar(48) NOT NULL,
  `operationCode` int NOT NULL,
  `approvedJobList` varchar(255) DEFAULT NULL,
  `approvingJobList` varchar(255) DEFAULT NULL,
  `numberOfValidDays` int NOT NULL DEFAULT '7',
  `defaultApproveText` varchar(48) NOT NULL DEFAULT 'Approved By',
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`schemaId`),
  UNIQUE KEY `schemaName` (`schemaName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_approvalSequenceSchema`
--

LOCK TABLES `_approvalSequenceSchema` WRITE;
/*!40000 ALTER TABLE `_approvalSequenceSchema` DISABLE KEYS */;
/*!40000 ALTER TABLE `_approvalSequenceSchema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_contextLookup`
--

DROP TABLE IF EXISTS `_contextLookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_contextLookup` (
  `contextId` int unsigned NOT NULL AUTO_INCREMENT,
  `symbol` char(1) NOT NULL,
  `_value` char(4) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`contextId`),
  UNIQUE KEY `_value` (`_value`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_contextLookup`
--

LOCK TABLES `_contextLookup` WRITE;
/*!40000 ALTER TABLE `_contextLookup` DISABLE KEYS */;
INSERT INTO `_contextLookup` VALUES (1,'A','0000',NULL,NULL,0),(2,'B','0001',NULL,NULL,0),(3,'C','0002',NULL,NULL,0),(4,'D','0010',NULL,NULL,0),(5,'E','0011',NULL,NULL,0),(6,'F','0012',NULL,NULL,0),(7,'G','0020',NULL,NULL,0),(8,'H','0021',NULL,NULL,0),(9,'I','0022',NULL,NULL,0),(10,'J','0100',NULL,NULL,0),(11,'K','0101',NULL,NULL,0),(12,'L','0102',NULL,NULL,0),(13,'M','0110',NULL,NULL,0),(14,'N','0111',NULL,NULL,0),(15,'O','0112',NULL,NULL,0),(16,'P','0120',NULL,NULL,0),(17,'Q','0121',NULL,NULL,0),(18,'R','0122',NULL,NULL,0),(19,'S','0200',NULL,NULL,0),(20,'T','0201',NULL,NULL,0),(21,'U','0202',NULL,NULL,0),(22,'V','0210',NULL,NULL,0),(23,'W','0211',NULL,NULL,0),(24,'X','0212',NULL,NULL,0),(25,'Y','0220',NULL,NULL,0),(26,'Z','0221',NULL,NULL,0),(27,'a','0222',NULL,NULL,0),(28,'b','1000',NULL,NULL,0),(29,'c','1001',NULL,NULL,0),(30,'d','1002',NULL,NULL,0),(31,'e','1010',NULL,NULL,0),(32,'f','1011',NULL,NULL,0),(33,'g','1012',NULL,NULL,0),(34,'h','1020',NULL,NULL,0),(35,'i','1021',NULL,NULL,0),(36,'j','1022',NULL,NULL,0),(37,'k','1100',NULL,NULL,0),(38,'l','1101',NULL,NULL,0),(39,'m','1102',NULL,NULL,0),(40,'n','1110',NULL,NULL,0),(41,'o','1111',NULL,NULL,0),(42,'p','1112',NULL,NULL,0),(43,'q','1120',NULL,NULL,0),(44,'r','1121',NULL,NULL,0),(45,'s','1122',NULL,NULL,0),(46,'t','1200',NULL,NULL,0),(47,'u','1201',NULL,NULL,0),(48,'v','1202',NULL,NULL,0),(49,'w','1210',NULL,NULL,0),(50,'x','1211',NULL,NULL,0),(51,'y','1212',NULL,NULL,0),(52,'z','1220',NULL,NULL,0),(53,'0','1221',NULL,NULL,0),(54,'1','1222',NULL,NULL,0),(55,'2','2000',NULL,NULL,0),(56,'3','2001',NULL,NULL,0),(57,'4','2002',NULL,NULL,0),(58,'5','2010',NULL,NULL,0),(59,'6','2011',NULL,NULL,0),(60,'7','2012',NULL,NULL,0),(61,'8','2020',NULL,NULL,0),(62,'9','2021',NULL,NULL,0),(63,'!','2022',NULL,NULL,0),(64,'@','2100',NULL,NULL,0),(65,'#','2101',NULL,NULL,0),(66,'%','2102',NULL,NULL,0),(67,'&','2110',NULL,NULL,0),(68,'(','2111',NULL,NULL,0),(69,')','2112',NULL,NULL,0),(70,'{','2120',NULL,NULL,0),(71,'}','2121',NULL,NULL,0),(72,'[','2122',NULL,NULL,0),(73,']','2200',NULL,NULL,0),(74,'<','2201',NULL,NULL,0),(75,'>','2202',NULL,NULL,0),(76,'?','2210',NULL,NULL,0),(77,'+','2211',NULL,NULL,0),(78,'*','2212',NULL,NULL,0),(79,'|','2220',NULL,NULL,0),(80,':','2221',NULL,NULL,0),(81,';','2222',NULL,NULL,0);
/*!40000 ALTER TABLE `_contextLookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_contextManager`
--

DROP TABLE IF EXISTS `_contextManager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_contextManager` (
  `defaultXValue` tinyint NOT NULL DEFAULT '1',
  PRIMARY KEY (`defaultXValue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_contextManager`
--

LOCK TABLES `_contextManager` WRITE;
/*!40000 ALTER TABLE `_contextManager` DISABLE KEYS */;
INSERT INTO `_contextManager` VALUES (1);
/*!40000 ALTER TABLE `_contextManager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_contextPosition`
--

DROP TABLE IF EXISTS `_contextPosition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_contextPosition` (
  `cId` int unsigned NOT NULL AUTO_INCREMENT,
  `cName` varchar(56) NOT NULL,
  `cPosition` int NOT NULL,
  `caption` varchar(96) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `extraInformation` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`cId`),
  UNIQUE KEY `cName` (`cName`),
  UNIQUE KEY `cPosition` (`cPosition`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_contextPosition`
--

LOCK TABLES `_contextPosition` WRITE;
/*!40000 ALTER TABLE `_contextPosition` DISABLE KEYS */;
INSERT INTO `_contextPosition` VALUES (1,'login',1,'User Accounts Management',NULL,NULL,0),(2,'login_create',2,'Create User Account',NULL,NULL,0),(3,'login_read',3,'View User Account',NULL,NULL,0),(4,'login_update',4,'Update User Account',NULL,NULL,0),(5,'login_delete',5,'Delete User Account',NULL,NULL,0),(6,'login_search',6,'Search User Accounts',NULL,NULL,0),(7,'profile',7,'General System Settings',NULL,NULL,0),(8,'profile_update',8,'Update System Settings',NULL,NULL,0),(9,'profile_read',9,'View System Settings',NULL,NULL,0),(10,'group',10,'General Group Operation',NULL,NULL,0),(11,'group_create',11,'Create a User Group',NULL,NULL,0),(12,'group_read',12,'View a User Group',NULL,NULL,0),(13,'group_update',13,'Update a User Group',NULL,NULL,0),(14,'group_delete',14,'Delete a User Group',NULL,NULL,0),(15,'group_search',15,'Search User Groups',NULL,NULL,0),(16,'jobtitle',16,'General Jobtitle Operations',NULL,NULL,0),(17,'jobtitle_create',17,'Create a User Jobtitle',NULL,NULL,0),(18,'jobtitle_read',18,'View a User Jobtitle',NULL,NULL,0),(19,'jobtitle_update',19,'Update a User Jobtitle',NULL,NULL,0),(20,'jobtitle_delete',20,'Delete a User Jobtitle',NULL,NULL,0),(21,'jobtitle_search',21,'Search User Jobtitles',NULL,NULL,0),(22,'menu_mysystem',22,'My System Menu',NULL,NULL,0),(23,'menu_users',23,'System Users',NULL,NULL,0),(24,'update_my_login',24,'Update My User Accounts',NULL,NULL,0),(25,'firewall',25,'Firewall/Authorization Management',NULL,NULL,0),(26,'firewall_update',26,'Update Firewall',NULL,NULL,0),(27,'firewall_read',27,'View Firewalls',NULL,NULL,0),(28,'system_lastresort_donotcare',28,'System Decision on Do not care',NULL,NULL,0),(29,'systemlogs',29,'General System Logs Management',NULL,NULL,0),(30,'systemlogs_search',30,'Search System Logs',NULL,NULL,0),(31,'notificationmanager',31,'Notification Manager Services',NULL,NULL,0),(32,'notificationmanager_create',32,'Create a New Notification Manager',NULL,NULL,0),(33,'notificationmanager_read',33,'Details/View a Notification Manager',NULL,NULL,0),(34,'notificationmanager_update',34,'Update a Notification Manager',NULL,NULL,0),(35,'notificationmanager_delete',35,'Delete a Notification Manager',NULL,NULL,0),(36,'notificationmanager_search',36,'Search a Notification Manager',NULL,NULL,0),(37,'systemuser',37,'General System User Management',NULL,NULL,0),(38,'systemuser_create',38,'Create a System User',NULL,NULL,0),(39,'systemuser_read',39,'Details/View of a System User',NULL,NULL,0),(40,'systemuser_update',40,'Update a System User',NULL,NULL,0),(41,'systemuser_delete',41,'Delete a System User',NULL,NULL,0),(42,'systemuser_search',42,'Search a System User',NULL,NULL,0),(43,'person',43,'General Person Management',NULL,NULL,0),(44,'person_create',44,'Create a Person',NULL,NULL,0),(45,'person_read',45,'Details/View of a Person',NULL,NULL,0),(46,'person_update',46,'Update a Person',NULL,NULL,0),(47,'person_delete',47,'Delete a Person',NULL,NULL,0),(48,'person_search',48,'Search a Person',NULL,NULL,0);
/*!40000 ALTER TABLE `_contextPosition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_country`
--

DROP TABLE IF EXISTS `_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_country` (
  `countryId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `countryName` varchar(48) NOT NULL,
  `abbreviation` varchar(4) NOT NULL,
  `code` int NOT NULL,
  `timezone` varchar(5) NOT NULL DEFAULT '03:00',
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`countryId`),
  UNIQUE KEY `countryName` (`countryName`)
) ENGINE=InnoDB AUTO_INCREMENT=259 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_country`
--

LOCK TABLES `_country` WRITE;
/*!40000 ALTER TABLE `_country` DISABLE KEYS */;
INSERT INTO `_country` VALUES (1,'0000:00:00:00:00:00','0000:00:00:00:00:00','Afghanistan','AF',255,'300',NULL,NULL,0),(2,'0000:00:00:00:00:00','0000:00:00:00:00:00','Akrotiri','0',255,'300',NULL,NULL,0),(3,'0000:00:00:00:00:00','0000:00:00:00:00:00','Albania','AL',255,'300',NULL,NULL,0),(4,'0000:00:00:00:00:00','0000:00:00:00:00:00','Algeria','DZ',255,'300',NULL,NULL,0),(5,'0000:00:00:00:00:00','0000:00:00:00:00:00','American Samoa','AS',255,'300',NULL,NULL,0),(6,'0000:00:00:00:00:00','0000:00:00:00:00:00','Andorra','AD',255,'300',NULL,NULL,0),(7,'0000:00:00:00:00:00','0000:00:00:00:00:00','Angola','AO',255,'300',NULL,NULL,0),(8,'0000:00:00:00:00:00','0000:00:00:00:00:00','Anguilla','AI',255,'300',NULL,NULL,0),(9,'0000:00:00:00:00:00','0000:00:00:00:00:00','Antarctica','AQ',255,'300',NULL,NULL,0),(10,'0000:00:00:00:00:00','0000:00:00:00:00:00','Antigua and Barbuda','AG',255,'300',NULL,NULL,0),(11,'0000:00:00:00:00:00','0000:00:00:00:00:00','Argentina','AR',255,'300',NULL,NULL,0),(12,'0000:00:00:00:00:00','0000:00:00:00:00:00','Armenia','AM',255,'300',NULL,NULL,0),(13,'0000:00:00:00:00:00','0000:00:00:00:00:00','Aruba','AW',255,'300',NULL,NULL,0),(14,'0000:00:00:00:00:00','0000:00:00:00:00:00','Ashmore and Cartier Islands','0',255,'300',NULL,NULL,0),(15,'0000:00:00:00:00:00','0000:00:00:00:00:00','Australia','AU',255,'300',NULL,NULL,0),(16,'0000:00:00:00:00:00','0000:00:00:00:00:00','Austria','AT',255,'300',NULL,NULL,0),(17,'0000:00:00:00:00:00','0000:00:00:00:00:00','Azerbaijan','0',255,'300',NULL,NULL,0),(18,'0000:00:00:00:00:00','0000:00:00:00:00:00','Bahamas The','BS',255,'300',NULL,NULL,0),(19,'0000:00:00:00:00:00','0000:00:00:00:00:00','Bahrain','BH',255,'300',NULL,NULL,0),(20,'0000:00:00:00:00:00','0000:00:00:00:00:00','Bangladesh','BD',255,'300',NULL,NULL,0),(21,'0000:00:00:00:00:00','0000:00:00:00:00:00','Barbados','BB',255,'300',NULL,NULL,0),(22,'0000:00:00:00:00:00','0000:00:00:00:00:00','Bassas da India','0',255,'300',NULL,NULL,0),(23,'0000:00:00:00:00:00','0000:00:00:00:00:00','Belarus','BY',255,'300',NULL,NULL,0),(24,'0000:00:00:00:00:00','0000:00:00:00:00:00','Belgium','BE',255,'300',NULL,NULL,0),(25,'0000:00:00:00:00:00','0000:00:00:00:00:00','Belize','BZ',255,'300',NULL,NULL,0),(26,'0000:00:00:00:00:00','0000:00:00:00:00:00','Benin','BJ',255,'300',NULL,NULL,0),(27,'0000:00:00:00:00:00','0000:00:00:00:00:00','Bermuda','BM',255,'300',NULL,NULL,0),(28,'0000:00:00:00:00:00','0000:00:00:00:00:00','Bhutan','BT',255,'300',NULL,NULL,0),(29,'0000:00:00:00:00:00','0000:00:00:00:00:00','Bolivia','BO',255,'300',NULL,NULL,0),(30,'0000:00:00:00:00:00','0000:00:00:00:00:00','Bosnia and Herzegovina','BA',255,'300',NULL,NULL,0),(31,'0000:00:00:00:00:00','0000:00:00:00:00:00','Botswana','BW',255,'300',NULL,NULL,0),(32,'0000:00:00:00:00:00','0000:00:00:00:00:00','Bouvet Island','BV',255,'300',NULL,NULL,0),(33,'0000:00:00:00:00:00','0000:00:00:00:00:00','Brazil','BR',255,'300',NULL,NULL,0),(34,'0000:00:00:00:00:00','0000:00:00:00:00:00','British Indian Ocean Territory','IO',255,'300',NULL,NULL,0),(35,'0000:00:00:00:00:00','0000:00:00:00:00:00','British Virgin Islands','0',255,'300',NULL,NULL,0),(36,'0000:00:00:00:00:00','0000:00:00:00:00:00','Brunei','BN',255,'300',NULL,NULL,0),(37,'0000:00:00:00:00:00','0000:00:00:00:00:00','Bulgaria','BG',255,'300',NULL,NULL,0),(38,'0000:00:00:00:00:00','0000:00:00:00:00:00','Burkina Faso','BF',255,'300',NULL,NULL,0),(39,'0000:00:00:00:00:00','0000:00:00:00:00:00','Burma','MM',255,'300',NULL,NULL,0),(40,'0000:00:00:00:00:00','0000:00:00:00:00:00','Burundi','BI',255,'300',NULL,NULL,0),(41,'0000:00:00:00:00:00','0000:00:00:00:00:00','Cambodia','KH',255,'300',NULL,NULL,0),(42,'0000:00:00:00:00:00','0000:00:00:00:00:00','Cameroon','CM',255,'300',NULL,NULL,0),(43,'0000:00:00:00:00:00','0000:00:00:00:00:00','Canada','CA',255,'300',NULL,NULL,0),(44,'0000:00:00:00:00:00','0000:00:00:00:00:00','Cape Verde','CV',255,'300',NULL,NULL,0),(45,'0000:00:00:00:00:00','0000:00:00:00:00:00','Cayman Islands','KY',255,'300',NULL,NULL,0),(46,'0000:00:00:00:00:00','0000:00:00:00:00:00','Central African Republic','CF',255,'300',NULL,NULL,0),(47,'0000:00:00:00:00:00','0000:00:00:00:00:00','Chad','TD',255,'300',NULL,NULL,0),(48,'0000:00:00:00:00:00','0000:00:00:00:00:00','Chile','CL',255,'300',NULL,NULL,0),(49,'0000:00:00:00:00:00','0000:00:00:00:00:00','China','CN',255,'300',NULL,NULL,0),(50,'0000:00:00:00:00:00','0000:00:00:00:00:00','Christmas Island','CX',255,'300',NULL,NULL,0),(51,'0000:00:00:00:00:00','0000:00:00:00:00:00','Clipperton Island','0',255,'300',NULL,NULL,0),(52,'0000:00:00:00:00:00','0000:00:00:00:00:00','Cocos (Keeling) Islands','CC',255,'300',NULL,NULL,0),(53,'0000:00:00:00:00:00','0000:00:00:00:00:00','Colombia','CO',255,'300',NULL,NULL,0),(54,'0000:00:00:00:00:00','0000:00:00:00:00:00','Comoros','KM',255,'300',NULL,NULL,0),(55,'0000:00:00:00:00:00','0000:00:00:00:00:00','Congo Democratic Republic of th','CD',255,'300',NULL,NULL,0),(56,'0000:00:00:00:00:00','0000:00:00:00:00:00','Congo Republic of the','CG',255,'300',NULL,NULL,0),(57,'0000:00:00:00:00:00','0000:00:00:00:00:00','Cook Islands','CK',255,'300',NULL,NULL,0),(58,'0000:00:00:00:00:00','0000:00:00:00:00:00','Coral Sea Islands','0',255,'300',NULL,NULL,0),(59,'0000:00:00:00:00:00','0000:00:00:00:00:00','Costa Rica','CR',255,'300',NULL,NULL,0),(60,'0000:00:00:00:00:00','0000:00:00:00:00:00','Cote d\'Ivoire','CI',255,'300',NULL,NULL,0),(61,'0000:00:00:00:00:00','0000:00:00:00:00:00','Croatia','HR',255,'300',NULL,NULL,0),(62,'0000:00:00:00:00:00','0000:00:00:00:00:00','Cuba','CU',255,'300',NULL,NULL,0),(63,'0000:00:00:00:00:00','0000:00:00:00:00:00','Cyprus','CY',255,'300',NULL,NULL,0),(64,'0000:00:00:00:00:00','0000:00:00:00:00:00','Czech Republic','CZ',255,'300',NULL,NULL,0),(65,'0000:00:00:00:00:00','0000:00:00:00:00:00','Denmark','DK',255,'300',NULL,NULL,0),(66,'0000:00:00:00:00:00','0000:00:00:00:00:00','Dhekelia','0',255,'300',NULL,NULL,0),(67,'0000:00:00:00:00:00','0000:00:00:00:00:00','Djibouti','DJ',255,'300',NULL,NULL,0),(68,'0000:00:00:00:00:00','0000:00:00:00:00:00','Dominica','DM',255,'300',NULL,NULL,0),(69,'0000:00:00:00:00:00','0000:00:00:00:00:00','Dominican Republic','DO',255,'300',NULL,NULL,0),(70,'0000:00:00:00:00:00','0000:00:00:00:00:00','Ecuador','EC',255,'300',NULL,NULL,0),(71,'0000:00:00:00:00:00','0000:00:00:00:00:00','Egypt','EG',255,'300',NULL,NULL,0),(72,'0000:00:00:00:00:00','0000:00:00:00:00:00','El Salvador','SV',255,'300',NULL,NULL,0),(73,'0000:00:00:00:00:00','0000:00:00:00:00:00','Equatorial Guinea','GQ',255,'300',NULL,NULL,0),(74,'0000:00:00:00:00:00','0000:00:00:00:00:00','Eritrea','ER',255,'300',NULL,NULL,0),(75,'0000:00:00:00:00:00','0000:00:00:00:00:00','Estonia','EE',255,'300',NULL,NULL,0),(76,'0000:00:00:00:00:00','0000:00:00:00:00:00','Ethiopia','ET',255,'300',NULL,NULL,0),(77,'0000:00:00:00:00:00','0000:00:00:00:00:00','Europa Island','0',255,'300',NULL,NULL,0),(78,'0000:00:00:00:00:00','0000:00:00:00:00:00','Falkland Islands (Islas Malvinas','FK',255,'300',NULL,NULL,0),(79,'0000:00:00:00:00:00','0000:00:00:00:00:00','Faroe Islands','FO',255,'300',NULL,NULL,0),(80,'0000:00:00:00:00:00','0000:00:00:00:00:00','Fiji','FJ',255,'300',NULL,NULL,0),(81,'0000:00:00:00:00:00','0000:00:00:00:00:00','Finland','FI',255,'300',NULL,NULL,0),(82,'0000:00:00:00:00:00','0000:00:00:00:00:00','France','FR',255,'300',NULL,NULL,0),(83,'0000:00:00:00:00:00','0000:00:00:00:00:00','French Guiana','GF',255,'300',NULL,NULL,0),(84,'0000:00:00:00:00:00','0000:00:00:00:00:00','French Polynesia','PF',255,'300',NULL,NULL,0),(85,'0000:00:00:00:00:00','0000:00:00:00:00:00','French Southern and Antarctic La','TF',255,'300',NULL,NULL,0),(86,'0000:00:00:00:00:00','0000:00:00:00:00:00','Gabon','GA',255,'300',NULL,NULL,0),(87,'0000:00:00:00:00:00','0000:00:00:00:00:00','Gambia The','GM',255,'300',NULL,NULL,0),(88,'0000:00:00:00:00:00','0000:00:00:00:00:00','Gaza Strip','0',255,'300',NULL,NULL,0),(89,'0000:00:00:00:00:00','0000:00:00:00:00:00','Georgia','GE',255,'300',NULL,NULL,0),(90,'0000:00:00:00:00:00','0000:00:00:00:00:00','Germany','DE',255,'300',NULL,NULL,0),(91,'0000:00:00:00:00:00','0000:00:00:00:00:00','Ghana','GH',255,'300',NULL,NULL,0),(92,'0000:00:00:00:00:00','0000:00:00:00:00:00','Gibraltar','GI',255,'300',NULL,NULL,0),(93,'0000:00:00:00:00:00','0000:00:00:00:00:00','Glorioso Islands','0',255,'300',NULL,NULL,0),(94,'0000:00:00:00:00:00','0000:00:00:00:00:00','Greece','GR',255,'300',NULL,NULL,0),(95,'0000:00:00:00:00:00','0000:00:00:00:00:00','Greenland','GL',255,'300',NULL,NULL,0),(96,'0000:00:00:00:00:00','0000:00:00:00:00:00','Grenada','GD',255,'300',NULL,NULL,0),(97,'0000:00:00:00:00:00','0000:00:00:00:00:00','Guadeloupe','GP',255,'300',NULL,NULL,0),(98,'0000:00:00:00:00:00','0000:00:00:00:00:00','Guam','GU',255,'300',NULL,NULL,0),(99,'0000:00:00:00:00:00','0000:00:00:00:00:00','Guatemala','GT',255,'300',NULL,NULL,0),(100,'0000:00:00:00:00:00','0000:00:00:00:00:00','Guernsey','0',255,'300',NULL,NULL,0),(101,'0000:00:00:00:00:00','0000:00:00:00:00:00','Guinea','GN',255,'300',NULL,NULL,0),(102,'0000:00:00:00:00:00','0000:00:00:00:00:00','Guinea-Bissau','GW',255,'300',NULL,NULL,0),(103,'0000:00:00:00:00:00','0000:00:00:00:00:00','Guyana','GY',255,'300',NULL,NULL,0),(104,'0000:00:00:00:00:00','0000:00:00:00:00:00','Haiti','HT',255,'300',NULL,NULL,0),(105,'0000:00:00:00:00:00','0000:00:00:00:00:00','Heard Island and McDonald Island','HM',255,'300',NULL,NULL,0),(106,'0000:00:00:00:00:00','0000:00:00:00:00:00','Holy See (Vatican City)','VA',255,'300',NULL,NULL,0),(107,'0000:00:00:00:00:00','0000:00:00:00:00:00','Honduras','HN',255,'300',NULL,NULL,0),(108,'0000:00:00:00:00:00','0000:00:00:00:00:00','Hong Kong','HK',255,'300',NULL,NULL,0),(109,'0000:00:00:00:00:00','0000:00:00:00:00:00','Hungary','HU',255,'300',NULL,NULL,0),(110,'0000:00:00:00:00:00','0000:00:00:00:00:00','Iceland','IS',255,'300',NULL,NULL,0),(111,'0000:00:00:00:00:00','0000:00:00:00:00:00','India','IN',255,'300',NULL,NULL,0),(112,'0000:00:00:00:00:00','0000:00:00:00:00:00','Indonesia','ID',255,'300',NULL,NULL,0),(113,'0000:00:00:00:00:00','0000:00:00:00:00:00','Iran','IR',255,'300',NULL,NULL,0),(114,'0000:00:00:00:00:00','0000:00:00:00:00:00','Iraq','IQ',255,'300',NULL,NULL,0),(115,'0000:00:00:00:00:00','0000:00:00:00:00:00','Ireland','IE',255,'300',NULL,NULL,0),(116,'0000:00:00:00:00:00','0000:00:00:00:00:00','Isle of Man','0',255,'300',NULL,NULL,0),(117,'0000:00:00:00:00:00','0000:00:00:00:00:00','Israel','IL',255,'300',NULL,NULL,0),(118,'0000:00:00:00:00:00','0000:00:00:00:00:00','Italy','IT',255,'300',NULL,NULL,0),(119,'0000:00:00:00:00:00','0000:00:00:00:00:00','Jamaica','JM',255,'300',NULL,NULL,0),(120,'0000:00:00:00:00:00','0000:00:00:00:00:00','Jan Mayen','0',255,'300',NULL,NULL,0),(121,'0000:00:00:00:00:00','0000:00:00:00:00:00','Japan','JP',255,'300',NULL,NULL,0),(122,'0000:00:00:00:00:00','0000:00:00:00:00:00','Jersey','0',255,'300',NULL,NULL,0),(123,'0000:00:00:00:00:00','0000:00:00:00:00:00','Jordan','JO',255,'300',NULL,NULL,0),(124,'0000:00:00:00:00:00','0000:00:00:00:00:00','Juan de Nova Island','0',255,'300',NULL,NULL,0),(125,'0000:00:00:00:00:00','0000:00:00:00:00:00','Kazakhstan','KZ',255,'300',NULL,NULL,0),(126,'0000:00:00:00:00:00','0000:00:00:00:00:00','Kenya','KE',255,'300',NULL,NULL,0),(127,'0000:00:00:00:00:00','0000:00:00:00:00:00','Kiribati','KI',255,'300',NULL,NULL,0),(128,'0000:00:00:00:00:00','0000:00:00:00:00:00','Korea North','0',255,'300',NULL,NULL,0),(129,'0000:00:00:00:00:00','0000:00:00:00:00:00','Korea South','0',255,'300',NULL,NULL,0),(130,'0000:00:00:00:00:00','0000:00:00:00:00:00','Kuwait','KP',255,'300',NULL,NULL,0),(131,'0000:00:00:00:00:00','0000:00:00:00:00:00','Kyrgyzstan','KG',255,'300',NULL,NULL,0),(132,'0000:00:00:00:00:00','0000:00:00:00:00:00','Laos','LA',255,'300',NULL,NULL,0),(133,'0000:00:00:00:00:00','0000:00:00:00:00:00','Latvia','LV',255,'300',NULL,NULL,0),(134,'0000:00:00:00:00:00','0000:00:00:00:00:00','Lebanon','LB',255,'300',NULL,NULL,0),(135,'0000:00:00:00:00:00','0000:00:00:00:00:00','Lesotho','LS',255,'300',NULL,NULL,0),(136,'0000:00:00:00:00:00','0000:00:00:00:00:00','Liberia','LR',255,'300',NULL,NULL,0),(137,'0000:00:00:00:00:00','0000:00:00:00:00:00','Libya','LY',255,'300',NULL,NULL,0),(138,'0000:00:00:00:00:00','0000:00:00:00:00:00','Liechtenstein','LI',255,'300',NULL,NULL,0),(139,'0000:00:00:00:00:00','0000:00:00:00:00:00','Lithuania','LT',255,'300',NULL,NULL,0),(140,'0000:00:00:00:00:00','0000:00:00:00:00:00','Luxembourg','LU',255,'300',NULL,NULL,0),(141,'0000:00:00:00:00:00','0000:00:00:00:00:00','Macau','MO',255,'300',NULL,NULL,0),(142,'0000:00:00:00:00:00','0000:00:00:00:00:00','Macedonia','MK',255,'300',NULL,NULL,0),(143,'0000:00:00:00:00:00','0000:00:00:00:00:00','Madagascar','MG',255,'300',NULL,NULL,0),(144,'0000:00:00:00:00:00','0000:00:00:00:00:00','Malawi','MW',255,'300',NULL,NULL,0),(145,'0000:00:00:00:00:00','0000:00:00:00:00:00','Malaysia','MY',255,'300',NULL,NULL,0),(146,'0000:00:00:00:00:00','0000:00:00:00:00:00','Maldives','MV',255,'300',NULL,NULL,0),(147,'0000:00:00:00:00:00','0000:00:00:00:00:00','Mali','ML',255,'300',NULL,NULL,0),(148,'0000:00:00:00:00:00','0000:00:00:00:00:00','Malta','MT',255,'300',NULL,NULL,0),(149,'0000:00:00:00:00:00','0000:00:00:00:00:00','Marshall Islands','MH',255,'300',NULL,NULL,0),(150,'0000:00:00:00:00:00','0000:00:00:00:00:00','Martinique','MQ',255,'300',NULL,NULL,0),(151,'0000:00:00:00:00:00','0000:00:00:00:00:00','Mauritania','MR',255,'300',NULL,NULL,0),(152,'0000:00:00:00:00:00','0000:00:00:00:00:00','Mauritius','MU',255,'300',NULL,NULL,0),(153,'0000:00:00:00:00:00','0000:00:00:00:00:00','Mayotte','YT',255,'300',NULL,NULL,0),(154,'0000:00:00:00:00:00','0000:00:00:00:00:00','Mexico','MX',255,'300',NULL,NULL,0),(155,'0000:00:00:00:00:00','0000:00:00:00:00:00','Micronesia Federated States of','FM',255,'300',NULL,NULL,0),(156,'0000:00:00:00:00:00','0000:00:00:00:00:00','Moldova','MD',255,'300',NULL,NULL,0),(157,'0000:00:00:00:00:00','0000:00:00:00:00:00','Monaco','MC',255,'300',NULL,NULL,0),(158,'0000:00:00:00:00:00','0000:00:00:00:00:00','Mongolia','MN',255,'300',NULL,NULL,0),(159,'0000:00:00:00:00:00','0000:00:00:00:00:00','Montserrat','MS',255,'300',NULL,NULL,0),(160,'0000:00:00:00:00:00','0000:00:00:00:00:00','Morocco','MA',255,'300',NULL,NULL,0),(161,'0000:00:00:00:00:00','0000:00:00:00:00:00','Mozambique','MZ',255,'300',NULL,NULL,0),(162,'0000:00:00:00:00:00','0000:00:00:00:00:00','Namibia','NA',255,'300',NULL,NULL,0),(163,'0000:00:00:00:00:00','0000:00:00:00:00:00','Nauru','NR',255,'300',NULL,NULL,0),(164,'0000:00:00:00:00:00','0000:00:00:00:00:00','Navassa Island','0',255,'300',NULL,NULL,0),(165,'0000:00:00:00:00:00','0000:00:00:00:00:00','Nepal','NP',255,'300',NULL,NULL,0),(166,'0000:00:00:00:00:00','0000:00:00:00:00:00','Netherlands','NL',255,'300',NULL,NULL,0),(167,'0000:00:00:00:00:00','0000:00:00:00:00:00','Netherlands Antilles','AN',255,'300',NULL,NULL,0),(168,'0000:00:00:00:00:00','0000:00:00:00:00:00','New Caledonia','NC',255,'300',NULL,NULL,0),(169,'0000:00:00:00:00:00','0000:00:00:00:00:00','New Zealand','NZ',255,'300',NULL,NULL,0),(170,'0000:00:00:00:00:00','0000:00:00:00:00:00','Nicaragua','NI',255,'300',NULL,NULL,0),(171,'0000:00:00:00:00:00','0000:00:00:00:00:00','Niger','NE',255,'300',NULL,NULL,0),(172,'0000:00:00:00:00:00','0000:00:00:00:00:00','Nigeria','NG',255,'300',NULL,NULL,0),(173,'0000:00:00:00:00:00','0000:00:00:00:00:00','Niue','NU',255,'300',NULL,NULL,0),(174,'0000:00:00:00:00:00','0000:00:00:00:00:00','Norfolk Island','NF',255,'300',NULL,NULL,0),(175,'0000:00:00:00:00:00','0000:00:00:00:00:00','Northern Mariana Islands','MP',255,'300',NULL,NULL,0),(176,'0000:00:00:00:00:00','0000:00:00:00:00:00','Norway','NO',255,'300',NULL,NULL,0),(177,'0000:00:00:00:00:00','0000:00:00:00:00:00','Oman','OM',255,'300',NULL,NULL,0),(178,'0000:00:00:00:00:00','0000:00:00:00:00:00','Pakistan','PK',255,'300',NULL,NULL,0),(179,'0000:00:00:00:00:00','0000:00:00:00:00:00','Palau','PW',255,'300',NULL,NULL,0),(180,'0000:00:00:00:00:00','0000:00:00:00:00:00','Panama','PA',255,'300',NULL,NULL,0),(181,'0000:00:00:00:00:00','0000:00:00:00:00:00','Papua New Guinea','PG',255,'300',NULL,NULL,0),(182,'0000:00:00:00:00:00','0000:00:00:00:00:00','Paracel Islands','0',255,'300',NULL,NULL,0),(183,'0000:00:00:00:00:00','0000:00:00:00:00:00','Paraguay','PY',255,'300',NULL,NULL,0),(184,'0000:00:00:00:00:00','0000:00:00:00:00:00','Peru','PE',255,'300',NULL,NULL,0),(185,'0000:00:00:00:00:00','0000:00:00:00:00:00','Philippines','PH',255,'300',NULL,NULL,0),(186,'0000:00:00:00:00:00','0000:00:00:00:00:00','Pitcairn Islands','PN',255,'300',NULL,NULL,0),(187,'0000:00:00:00:00:00','0000:00:00:00:00:00','Poland','PL',255,'300',NULL,NULL,0),(188,'0000:00:00:00:00:00','0000:00:00:00:00:00','Portugal','PT',255,'300',NULL,NULL,0),(189,'0000:00:00:00:00:00','0000:00:00:00:00:00','Puerto Rico','PR',255,'300',NULL,NULL,0),(190,'0000:00:00:00:00:00','0000:00:00:00:00:00','Qatar','QA',255,'300',NULL,NULL,0),(191,'0000:00:00:00:00:00','0000:00:00:00:00:00','Reunion','RE',255,'300',NULL,NULL,0),(192,'0000:00:00:00:00:00','0000:00:00:00:00:00','Romania','RO',255,'300',NULL,NULL,0),(193,'0000:00:00:00:00:00','0000:00:00:00:00:00','Russia','RU',255,'300',NULL,NULL,0),(194,'0000:00:00:00:00:00','0000:00:00:00:00:00','Rwanda','RW',255,'300',NULL,NULL,0),(195,'0000:00:00:00:00:00','0000:00:00:00:00:00','Saint Helena','0',255,'300',NULL,NULL,0),(196,'0000:00:00:00:00:00','0000:00:00:00:00:00','Saint Kitts and Nevis','KN',255,'300',NULL,NULL,0),(197,'0000:00:00:00:00:00','0000:00:00:00:00:00','Saint Lucia','LC',255,'300',NULL,NULL,0),(198,'0000:00:00:00:00:00','0000:00:00:00:00:00','Saint Pierre and Miquelon','0',255,'300',NULL,NULL,0),(199,'0000:00:00:00:00:00','0000:00:00:00:00:00','Saint Vincent and the Grenadines','VC',255,'300',NULL,NULL,0),(200,'0000:00:00:00:00:00','0000:00:00:00:00:00','Samoa','WS',255,'300',NULL,NULL,0),(201,'0000:00:00:00:00:00','0000:00:00:00:00:00','San Marino','SM',255,'300',NULL,NULL,0),(202,'0000:00:00:00:00:00','0000:00:00:00:00:00','Sao Tome and Principe','ST',255,'300',NULL,NULL,0),(203,'0000:00:00:00:00:00','0000:00:00:00:00:00','Saudi Arabia','SA',255,'300',NULL,NULL,0),(204,'0000:00:00:00:00:00','0000:00:00:00:00:00','Senegal','SN',255,'300',NULL,NULL,0),(205,'0000:00:00:00:00:00','0000:00:00:00:00:00','Serbia and Montenegro','RS',255,'300',NULL,NULL,0),(206,'0000:00:00:00:00:00','0000:00:00:00:00:00','Seychelles','SC',255,'300',NULL,NULL,0),(207,'0000:00:00:00:00:00','0000:00:00:00:00:00','Sierra Leone','SL',255,'300',NULL,NULL,0),(208,'0000:00:00:00:00:00','0000:00:00:00:00:00','Singapore','SG',255,'300',NULL,NULL,0),(209,'0000:00:00:00:00:00','0000:00:00:00:00:00','Slovakia','SK',255,'300',NULL,NULL,0),(210,'0000:00:00:00:00:00','0000:00:00:00:00:00','Slovenia','SI',255,'300',NULL,NULL,0),(211,'0000:00:00:00:00:00','0000:00:00:00:00:00','Solomon Islands','SB',255,'300',NULL,NULL,0),(212,'0000:00:00:00:00:00','0000:00:00:00:00:00','Somalia','SO',255,'300',NULL,NULL,0),(213,'0000:00:00:00:00:00','0000:00:00:00:00:00','South Africa','ZA',255,'300',NULL,NULL,0),(214,'0000:00:00:00:00:00','0000:00:00:00:00:00','South Georgia and the South Sand','GS',255,'300',NULL,NULL,0),(215,'0000:00:00:00:00:00','0000:00:00:00:00:00','Spain','ES',255,'300',NULL,NULL,0),(216,'0000:00:00:00:00:00','0000:00:00:00:00:00','Spratly Islands','0',255,'300',NULL,NULL,0),(217,'0000:00:00:00:00:00','0000:00:00:00:00:00','Sri Lanka','LK',255,'300',NULL,NULL,0),(218,'0000:00:00:00:00:00','0000:00:00:00:00:00','Sudan','SD',255,'300',NULL,NULL,0),(219,'0000:00:00:00:00:00','0000:00:00:00:00:00','Suriname','SR',255,'300',NULL,NULL,0),(220,'0000:00:00:00:00:00','0000:00:00:00:00:00','Svalbard','SJ',255,'300',NULL,NULL,0),(221,'0000:00:00:00:00:00','0000:00:00:00:00:00','Swaziland','SZ',255,'300',NULL,NULL,0),(222,'0000:00:00:00:00:00','0000:00:00:00:00:00','Sweden','SE',255,'300',NULL,NULL,0),(223,'0000:00:00:00:00:00','0000:00:00:00:00:00','Switzerland','CH',255,'300',NULL,NULL,0),(224,'0000:00:00:00:00:00','0000:00:00:00:00:00','Syria','SY',255,'300',NULL,NULL,0),(225,'0000:00:00:00:00:00','0000:00:00:00:00:00','Taiwan','TW',255,'300',NULL,NULL,0),(226,'0000:00:00:00:00:00','0000:00:00:00:00:00','Tajikistan','TJ',255,'300',NULL,NULL,0),(227,'0000:00:00:00:00:00','0000:00:00:00:00:00','Tanzania','TZ',255,'300',NULL,NULL,0),(228,'0000:00:00:00:00:00','0000:00:00:00:00:00','Thailand','TH',255,'300',NULL,NULL,0),(229,'0000:00:00:00:00:00','0000:00:00:00:00:00','Timor-Leste','0',255,'300',NULL,NULL,0),(230,'0000:00:00:00:00:00','0000:00:00:00:00:00','Togo','TG',255,'300',NULL,NULL,0),(231,'0000:00:00:00:00:00','0000:00:00:00:00:00','Tokelau','TK',255,'300',NULL,NULL,0),(232,'0000:00:00:00:00:00','0000:00:00:00:00:00','Tonga','TO',255,'300',NULL,NULL,0),(233,'0000:00:00:00:00:00','0000:00:00:00:00:00','Trinidad and Tobago','TT',255,'300',NULL,NULL,0),(234,'0000:00:00:00:00:00','0000:00:00:00:00:00','Tromelin Island','0',255,'300',NULL,NULL,0),(235,'0000:00:00:00:00:00','0000:00:00:00:00:00','Tunisia','TN',255,'300',NULL,NULL,0),(236,'0000:00:00:00:00:00','0000:00:00:00:00:00','Turkey','TR',255,'300',NULL,NULL,0),(237,'0000:00:00:00:00:00','0000:00:00:00:00:00','Turkmenistan','TM',255,'300',NULL,NULL,0),(238,'0000:00:00:00:00:00','0000:00:00:00:00:00','Turks and Caicos Islands','TC',255,'300',NULL,NULL,0),(239,'0000:00:00:00:00:00','0000:00:00:00:00:00','Tuvalu','TV',255,'300',NULL,NULL,0),(240,'0000:00:00:00:00:00','0000:00:00:00:00:00','Uganda','UG',255,'300',NULL,NULL,0),(241,'0000:00:00:00:00:00','0000:00:00:00:00:00','Ukraine','UA',255,'300',NULL,NULL,0),(242,'0000:00:00:00:00:00','0000:00:00:00:00:00','United Arab Emirates','AE',255,'300',NULL,NULL,0),(243,'0000:00:00:00:00:00','0000:00:00:00:00:00','United Kingdom','GB',255,'300',NULL,NULL,0),(244,'0000:00:00:00:00:00','0000:00:00:00:00:00','United States','US',255,'300',NULL,NULL,0),(245,'0000:00:00:00:00:00','0000:00:00:00:00:00','Uruguay','UY',255,'300',NULL,NULL,0),(246,'0000:00:00:00:00:00','0000:00:00:00:00:00','Uzbekistan','UZ',255,'300',NULL,NULL,0),(247,'0000:00:00:00:00:00','0000:00:00:00:00:00','Vanuatu','VU',255,'300',NULL,NULL,0),(248,'0000:00:00:00:00:00','0000:00:00:00:00:00','Venezuela','VE',255,'300',NULL,NULL,0),(249,'0000:00:00:00:00:00','0000:00:00:00:00:00','Vietnam','VN',255,'300',NULL,NULL,0),(250,'0000:00:00:00:00:00','0000:00:00:00:00:00','Virgin Islands','VI',255,'300',NULL,NULL,0),(251,'0000:00:00:00:00:00','0000:00:00:00:00:00','Wake Island','0',255,'300',NULL,NULL,0),(252,'0000:00:00:00:00:00','0000:00:00:00:00:00','Wallis and Futuna','WF',255,'300',NULL,NULL,0),(253,'0000:00:00:00:00:00','0000:00:00:00:00:00','West Bank','0',255,'300',NULL,NULL,0),(254,'0000:00:00:00:00:00','0000:00:00:00:00:00','Western Sahara','EH',255,'300',NULL,NULL,0),(255,'0000:00:00:00:00:00','0000:00:00:00:00:00','Yemen','YE',255,'300',NULL,NULL,0),(256,'0000:00:00:00:00:00','0000:00:00:00:00:00','Zambia','ZM',255,'300',NULL,NULL,0),(257,'0000:00:00:00:00:00','0000:00:00:00:00:00','Zimbabwe','ZW',255,'300',NULL,NULL,0),(258,'0000:00:00:00:00:00','0000:00:00:00:00:00','South Sudan','SS',255,'300',NULL,NULL,0);
/*!40000 ALTER TABLE `_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_daysOfAWeek`
--

DROP TABLE IF EXISTS `_daysOfAWeek`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_daysOfAWeek` (
  `dayId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `dayName` varchar(9) NOT NULL,
  `abbreviation` varchar(4) NOT NULL,
  `offsetValue` int NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`dayId`),
  UNIQUE KEY `dayName` (`dayName`),
  UNIQUE KEY `abbreviation` (`abbreviation`),
  UNIQUE KEY `offsetValue` (`offsetValue`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_daysOfAWeek`
--

LOCK TABLES `_daysOfAWeek` WRITE;
/*!40000 ALTER TABLE `_daysOfAWeek` DISABLE KEYS */;
INSERT INTO `_daysOfAWeek` VALUES (1,'0000:00:00:00:00:00','0000:00:00:00:00:00','Sunday','Sun',0,NULL,NULL,0),(2,'0000:00:00:00:00:00','0000:00:00:00:00:00','Monday','Mon',1,NULL,NULL,0),(3,'0000:00:00:00:00:00','0000:00:00:00:00:00','Tuesday','Tue',2,NULL,NULL,0),(4,'0000:00:00:00:00:00','0000:00:00:00:00:00','Wednesday','Wed',3,NULL,NULL,0),(5,'0000:00:00:00:00:00','0000:00:00:00:00:00','Thursday','Thur',4,NULL,NULL,0),(6,'0000:00:00:00:00:00','0000:00:00:00:00:00','Friday','Fri',5,NULL,NULL,0),(7,'0000:00:00:00:00:00','0000:00:00:00:00:00','Saturday','Sat',6,NULL,NULL,0);
/*!40000 ALTER TABLE `_daysOfAWeek` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_groups`
--

DROP TABLE IF EXISTS `_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_groups` (
  `groupId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `groupName` varchar(96) NOT NULL,
  `pId` int unsigned DEFAULT NULL,
  `context` char(255) NOT NULL DEFAULT ';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;',
  `root` tinyint NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`groupId`),
  UNIQUE KEY `groupName` (`groupName`),
  KEY `pId` (`pId`),
  CONSTRAINT `_groups_ibfk_1` FOREIGN KEY (`pId`) REFERENCES `_groups` (`groupId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_groups`
--

LOCK TABLES `_groups` WRITE;
/*!40000 ALTER TABLE `_groups` DISABLE KEYS */;
INSERT INTO `_groups` VALUES (1,'0000:00:00:00:00:00','0000:00:00:00:00:00','General System',NULL,';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;',1,NULL,NULL,0);
/*!40000 ALTER TABLE `_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_jobTitle`
--

DROP TABLE IF EXISTS `_jobTitle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_jobTitle` (
  `jobId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `jobName` varchar(64) NOT NULL,
  `context` char(255) NOT NULL DEFAULT ';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;',
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`jobId`),
  UNIQUE KEY `jobName` (`jobName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_jobTitle`
--

LOCK TABLES `_jobTitle` WRITE;
/*!40000 ALTER TABLE `_jobTitle` DISABLE KEYS */;
INSERT INTO `_jobTitle` VALUES (1,'0000:00:00:00:00:00','0000:00:00:00:00:00','System User',';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;',NULL,NULL,0);
/*!40000 ALTER TABLE `_jobTitle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_language`
--

DROP TABLE IF EXISTS `_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_language` (
  `languageId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `languageName` varchar(32) NOT NULL,
  `code` varchar(8) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`languageId`),
  UNIQUE KEY `languageName` (`languageName`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_language`
--

LOCK TABLES `_language` WRITE;
/*!40000 ALTER TABLE `_language` DISABLE KEYS */;
INSERT INTO `_language` VALUES (1,'0000:00:00:00:00:00','0000:00:00:00:00:00','English','En',NULL,NULL,0),(2,'0000:00:00:00:00:00','0000:00:00:00:00:00','Kiswahili','Sw',NULL,NULL,0);
/*!40000 ALTER TABLE `_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_login`
--

DROP TABLE IF EXISTS `_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_login` (
  `loginId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `loginName` varchar(56) NOT NULL,
  `password` char(40) DEFAULT NULL,
  `fullName` varchar(48) DEFAULT NULL,
  `groupId` int unsigned DEFAULT NULL,
  `sexId` int unsigned DEFAULT NULL,
  `maritalId` int unsigned DEFAULT NULL,
  `context` char(255) NOT NULL DEFAULT ';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;',
  `jobId` int unsigned DEFAULT NULL,
  `root` tinyint NOT NULL DEFAULT '0',
  `initializationCounter` int NOT NULL DEFAULT '1',
  `phone` varchar(16) DEFAULT NULL,
  `email` varchar(32) NOT NULL,
  `statusId` int unsigned NOT NULL DEFAULT '1',
  `userType` int NOT NULL DEFAULT '1',
  `userRef` varchar(64) NOT NULL,
  `photo` varchar(64) DEFAULT NULL,
  `qnSecurity1` int unsigned DEFAULT NULL,
  `ansSecurity1` varchar(32) DEFAULT NULL,
  `qnSecurity2` int unsigned DEFAULT NULL,
  `ansSecurity2` varchar(32) DEFAULT NULL,
  `firstDayOfAWeekId` int unsigned DEFAULT NULL,
  `randomNumber` char(32) DEFAULT NULL,
  `lastLoginTime` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`loginId`),
  UNIQUE KEY `loginName` (`loginName`),
  UNIQUE KEY `email` (`email`),
  KEY `groupId` (`groupId`),
  KEY `sexId` (`sexId`),
  KEY `maritalId` (`maritalId`),
  KEY `jobId` (`jobId`),
  KEY `statusId` (`statusId`),
  KEY `qnSecurity1` (`qnSecurity1`),
  KEY `qnSecurity2` (`qnSecurity2`),
  KEY `firstDayOfAWeekId` (`firstDayOfAWeekId`),
  CONSTRAINT `_login_ibfk_1` FOREIGN KEY (`groupId`) REFERENCES `_groups` (`groupId`),
  CONSTRAINT `_login_ibfk_2` FOREIGN KEY (`sexId`) REFERENCES `_sex` (`sexId`),
  CONSTRAINT `_login_ibfk_3` FOREIGN KEY (`maritalId`) REFERENCES `_marital` (`maritalId`),
  CONSTRAINT `_login_ibfk_4` FOREIGN KEY (`jobId`) REFERENCES `_jobTitle` (`jobId`),
  CONSTRAINT `_login_ibfk_5` FOREIGN KEY (`statusId`) REFERENCES `_userStatus` (`statusId`),
  CONSTRAINT `_login_ibfk_6` FOREIGN KEY (`qnSecurity1`) REFERENCES `_securityQuestion` (`questionId`),
  CONSTRAINT `_login_ibfk_7` FOREIGN KEY (`qnSecurity2`) REFERENCES `_securityQuestion` (`questionId`),
  CONSTRAINT `_login_ibfk_8` FOREIGN KEY (`firstDayOfAWeekId`) REFERENCES `_daysOfAWeek` (`dayId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_login`
--

LOCK TABLES `_login` WRITE;
/*!40000 ALTER TABLE `_login` DISABLE KEYS */;
INSERT INTO `_login` VALUES (1,'0000:00:00:00:00:00','0000:00:00:00:00:00','ndimangwa','d033e22ae348aeb5660fc2140aec35850c4da997','Ndimangwa Fadhili Ngoya',1,1,2,';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;',1,1,1,'+255787101808','ndimangwa@gmail.com',1,1,'SystemUser.1',NULL,1,'Default Answer',2,'Default Answer',1,NULL,'0000:00:00:00:00:00',NULL,NULL,0),(2,'0000:00:00:00:00:00','0000:00:00:00:00:00','valentina','d033e22ae348aeb5660fc2140aec35850c4da997','Valentina Nyamanoko Bwire',1,2,2,';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;',1,1,1,'+255784808101','tsbwire@gmail.com',1,1,'SystemUser.2',NULL,1,'Default Answer',2,'Default Answer',1,NULL,'0000:00:00:00:00:00',NULL,NULL,0);
/*!40000 ALTER TABLE `_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_marital`
--

DROP TABLE IF EXISTS `_marital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_marital` (
  `maritalId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `maritalName` varchar(24) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`maritalId`),
  UNIQUE KEY `maritalName` (`maritalName`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_marital`
--

LOCK TABLES `_marital` WRITE;
/*!40000 ALTER TABLE `_marital` DISABLE KEYS */;
INSERT INTO `_marital` VALUES (1,'0000:00:00:00:00:00','0000:00:00:00:00:00','Single',NULL,NULL,0),(2,'0000:00:00:00:00:00','0000:00:00:00:00:00','Married',NULL,NULL,0),(3,'0000:00:00:00:00:00','0000:00:00:00:00:00','Divorced',NULL,NULL,0),(4,'0000:00:00:00:00:00','0000:00:00:00:00:00','Widowed',NULL,NULL,0),(5,'0000:00:00:00:00:00','0000:00:00:00:00:00','Widower',NULL,NULL,0),(6,'0000:00:00:00:00:00','0000:00:00:00:00:00','Cohabiting',NULL,NULL,0),(7,'0000:00:00:00:00:00','0000:00:00:00:00:00','Civil Union',NULL,NULL,0),(8,'0000:00:00:00:00:00','0000:00:00:00:00:00','Domestic Partner',NULL,NULL,0),(9,'0000:00:00:00:00:00','0000:00:00:00:00:00','Unmarried Partner',NULL,NULL,0);
/*!40000 ALTER TABLE `_marital` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_monthOfAYear`
--

DROP TABLE IF EXISTS `_monthOfAYear`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_monthOfAYear` (
  `monthId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `monthName` varchar(9) NOT NULL,
  `abbreviation` varchar(3) NOT NULL,
  `number` int NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`monthId`),
  UNIQUE KEY `monthName` (`monthName`),
  UNIQUE KEY `abbreviation` (`abbreviation`),
  UNIQUE KEY `number` (`number`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_monthOfAYear`
--

LOCK TABLES `_monthOfAYear` WRITE;
/*!40000 ALTER TABLE `_monthOfAYear` DISABLE KEYS */;
INSERT INTO `_monthOfAYear` VALUES (1,'0000:00:00:00:00:00','0000:00:00:00:00:00','January','Jan',1,NULL,NULL,0),(2,'0000:00:00:00:00:00','0000:00:00:00:00:00','February','Feb',2,NULL,NULL,0),(3,'0000:00:00:00:00:00','0000:00:00:00:00:00','March','Mar',3,NULL,NULL,0),(4,'0000:00:00:00:00:00','0000:00:00:00:00:00','April','Apr',4,NULL,NULL,0),(5,'0000:00:00:00:00:00','0000:00:00:00:00:00','May','May',5,NULL,NULL,0),(6,'0000:00:00:00:00:00','0000:00:00:00:00:00','June','Jun',6,NULL,NULL,0),(7,'0000:00:00:00:00:00','0000:00:00:00:00:00','July','Jul',7,NULL,NULL,0),(8,'0000:00:00:00:00:00','0000:00:00:00:00:00','August','Aug',8,NULL,NULL,0),(9,'0000:00:00:00:00:00','0000:00:00:00:00:00','September','Sep',9,NULL,NULL,0),(10,'0000:00:00:00:00:00','0000:00:00:00:00:00','October','Oct',10,NULL,NULL,0),(11,'0000:00:00:00:00:00','0000:00:00:00:00:00','November','Nov',11,NULL,NULL,0),(12,'0000:00:00:00:00:00','0000:00:00:00:00:00','December','Dec',12,NULL,NULL,0);
/*!40000 ALTER TABLE `_monthOfAYear` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_navigationMenu`
--

DROP TABLE IF EXISTS `_navigationMenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_navigationMenu` (
  `menuId` int unsigned NOT NULL AUTO_INCREMENT,
  `menuName` varchar(255) NOT NULL,
  `pageToGo` varchar(255) DEFAULT NULL,
  `seqno` int DEFAULT '0',
  `pId` int unsigned DEFAULT NULL,
  `cName` varchar(56) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`menuId`),
  KEY `pId` (`pId`),
  CONSTRAINT `_navigationMenu_ibfk_1` FOREIGN KEY (`pId`) REFERENCES `_navigationMenu` (`menuId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_navigationMenu`
--

LOCK TABLES `_navigationMenu` WRITE;
/*!40000 ALTER TABLE `_navigationMenu` DISABLE KEYS */;
INSERT INTO `_navigationMenu` VALUES (1,'Staff/User Management','',0,NULL,'menu_users',0),(2,'Patient Management','',0,NULL,'menu_patients',0),(3,'Financial Management','',0,NULL,'finance',0),(4,'Patient Flow Management','',0,NULL,'patient_flow',0),(5,'Laboratory Management','',0,NULL,'laboratory',0),(6,'Admission Management','',0,NULL,'admission',0),(7,'Pharmacy Management','',0,NULL,'pharmacy',0),(8,'Ward & Accomodation Management','',0,NULL,'accomodation',0),(9,'User Accounts Management','page=login',0,1,'login',0),(10,'Job Title Management','page=jobtitle',0,1,'jobtitle',0),(11,'Group Management','page=group',0,1,'group',0);
/*!40000 ALTER TABLE `_navigationMenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_notification`
--

DROP TABLE IF EXISTS `_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_notification` (
  `notificationId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `caption` varchar(64) NOT NULL,
  `targetRef` varchar(64) DEFAULT NULL,
  `numberOfValidDays` int NOT NULL DEFAULT '30',
  `categoryId` int unsigned NOT NULL,
  `forwardURL` varchar(128) DEFAULT NULL,
  `closed` tinyint NOT NULL DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`notificationId`),
  KEY `categoryId` (`categoryId`),
  CONSTRAINT `_notification_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `_notificationCategory` (`categoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_notification`
--

LOCK TABLES `_notification` WRITE;
/*!40000 ALTER TABLE `_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_notificationCategory`
--

DROP TABLE IF EXISTS `_notificationCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_notificationCategory` (
  `categoryId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `categoryName` varchar(32) NOT NULL,
  `icons` varchar(64) DEFAULT NULL,
  `priority` int NOT NULL DEFAULT '3',
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`categoryId`),
  UNIQUE KEY `categoryName` (`categoryName`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_notificationCategory`
--

LOCK TABLES `_notificationCategory` WRITE;
/*!40000 ALTER TABLE `_notificationCategory` DISABLE KEYS */;
INSERT INTO `_notificationCategory` VALUES (1,'0000:00:00:00:00:00','0000:00:00:00:00:00','Invoice Raised',NULL,3,NULL,NULL,0),(2,'0000:00:00:00:00:00','0000:00:00:00:00:00','Invoice Cancelled',NULL,3,NULL,NULL,0),(3,'0000:00:00:00:00:00','0000:00:00:00:00:00','Receipt Issued',NULL,3,NULL,NULL,0),(4,'0000:00:00:00:00:00','0000:00:00:00:00:00','Appointment Due',NULL,3,NULL,NULL,0),(5,'0000:00:00:00:00:00','0000:00:00:00:00:00','Emergency',NULL,7,NULL,NULL,0);
/*!40000 ALTER TABLE `_notificationCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_notificationManager`
--

DROP TABLE IF EXISTS `_notificationManager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_notificationManager` (
  `managerId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `managerName` varchar(64) NOT NULL,
  `opname` varchar(56) NOT NULL,
  `targetRef` varchar(64) DEFAULT NULL,
  `categoryId` int unsigned NOT NULL,
  `numberOfValidDays` int NOT NULL DEFAULT '100',
  `forwardURL` varchar(128) DEFAULT NULL,
  `URLArgs` varchar(128) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`managerId`),
  KEY `categoryId` (`categoryId`),
  CONSTRAINT `_notificationManager_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `_notificationCategory` (`categoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_notificationManager`
--

LOCK TABLES `_notificationManager` WRITE;
/*!40000 ALTER TABLE `_notificationManager` DISABLE KEYS */;
/*!40000 ALTER TABLE `_notificationManager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_pHPTimezone`
--

DROP TABLE IF EXISTS `_pHPTimezone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_pHPTimezone` (
  `zoneId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `zoneName` varchar(48) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`zoneId`),
  UNIQUE KEY `zoneName` (`zoneName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_pHPTimezone`
--

LOCK TABLES `_pHPTimezone` WRITE;
/*!40000 ALTER TABLE `_pHPTimezone` DISABLE KEYS */;
INSERT INTO `_pHPTimezone` VALUES (1,'0000:00:00:00:00:00','0000:00:00:00:00:00','africa/dar_es_salaam',NULL,NULL,0);
/*!40000 ALTER TABLE `_pHPTimezone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_person`
--

DROP TABLE IF EXISTS `_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_person` (
  `personId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `personName` varchar(64) NOT NULL,
  `sexId` int unsigned NOT NULL,
  `fatherId` int unsigned DEFAULT NULL,
  `motherId` int unsigned DEFAULT NULL,
  `listOfSpouses` varchar(24) DEFAULT NULL,
  `ageOfFatherDuringBirth` int DEFAULT '0',
  `ageOfMotherDuringBirth` int DEFAULT '0',
  `yearOfBirth` int DEFAULT '0',
  `yearOfDeath` int NOT NULL DEFAULT '0',
  `totalYearsLived` int DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`personId`),
  KEY `sexId` (`sexId`),
  KEY `fatherId` (`fatherId`),
  KEY `motherId` (`motherId`),
  CONSTRAINT `_person_ibfk_1` FOREIGN KEY (`sexId`) REFERENCES `_sex` (`sexId`),
  CONSTRAINT `_person_ibfk_2` FOREIGN KEY (`fatherId`) REFERENCES `_person` (`personId`),
  CONSTRAINT `_person_ibfk_3` FOREIGN KEY (`motherId`) REFERENCES `_person` (`personId`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_person`
--

LOCK TABLES `_person` WRITE;
/*!40000 ALTER TABLE `_person` DISABLE KEYS */;
INSERT INTO `_person` VALUES (1,'0000:00:00:00:00:00','0000:00:00:00:00:00','Adam',1,NULL,NULL,NULL,0,0,1,931,930,'nz8bV6ZScO8u0gvKKyXy65bCgp1gMCvG',NULL,0),(2,'0000:00:00:00:00:00','0000:00:00:00:00:00','Eve',2,NULL,NULL,NULL,0,0,1,0,0,NULL,NULL,0),(3,'2021:11:14:14:00:38','2021:11:14:14:00:38','Seth',1,1,2,NULL,130,0,131,1043,912,NULL,'Genesis 1:3,8',0),(4,'2021:11:14:14:02:11','2021:11:14:14:02:11','Enos',1,3,NULL,NULL,105,0,236,1141,905,NULL,'Genesis 1:6,11',0),(5,'2021:11:14:14:03:32','2021:11:14:14:03:32','Cainan',1,4,NULL,NULL,90,0,326,1236,910,NULL,'Genesis 1:9,14',0),(6,'2021:11:14:14:05:25','2021:11:14:14:05:25','Mahalaleel',1,5,NULL,NULL,70,0,396,1291,895,NULL,'Genesis 1:12,17',0),(7,'2021:11:14:14:06:53','2021:11:14:14:06:53','Jared',1,6,NULL,NULL,65,0,461,1423,962,NULL,'Genesis 1:15,20',0),(8,'2021:11:14:14:08:46','2021:11:14:14:08:46','Enoch',1,7,NULL,NULL,162,0,623,988,365,NULL,'Genesis 1:18,23',0),(9,'2021:11:14:14:10:11','2021:11:14:14:10:11','Methuselah',1,8,NULL,NULL,65,0,688,1657,969,NULL,'Genesis 1:21,27',0),(10,'2021:11:14:14:12:03','2021:11:14:14:12:03','Lamech',1,9,NULL,NULL,187,0,875,1652,777,NULL,'Genesis 1:25,31',0),(11,'2021:11:14:14:13:44','2021:11:14:14:13:44','Noah',1,10,NULL,NULL,182,0,1057,0,0,NULL,'Genesis 1:28,29',0),(12,'2021:11:14:14:14:27','2021:11:14:14:14:27','Shem',1,11,NULL,NULL,500,0,1557,0,0,NULL,'Genesis 1:32',0),(13,'2021:11:14:14:14:54','2021:11:14:14:14:54','Ham',1,11,NULL,NULL,500,0,1557,0,0,NULL,'Genesis 1:32',0),(14,'2021:11:14:14:15:22','2021:11:14:14:15:22','Japeth',1,11,NULL,NULL,500,0,1557,0,0,NULL,'Genesis 1:32',0);
/*!40000 ALTER TABLE `_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_profile`
--

DROP TABLE IF EXISTS `_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_profile` (
  `profileId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `profileName` varchar(96) NOT NULL,
  `systemName` varchar(108) NOT NULL,
  `logo` varchar(64) DEFAULT NULL,
  `telephoneList` varchar(255) DEFAULT NULL,
  `emailList` varchar(32) DEFAULT NULL,
  `otherCommunication` varchar(255) DEFAULT NULL,
  `website` varchar(64) DEFAULT NULL,
  `fax` varchar(32) DEFAULT NULL,
  `address` varchar(64) DEFAULT NULL,
  `city` varchar(32) NOT NULL,
  `region` varchar(32) NOT NULL,
  `countryId` int unsigned DEFAULT NULL,
  `pHPTimezoneId` int unsigned DEFAULT NULL,
  `dob` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `firstDayOfAWeekId` int unsigned DEFAULT NULL,
  `xmlFile` varchar(64) DEFAULT NULL,
  `xmlFileChecksum` char(32) DEFAULT NULL,
  `serverIP4Address` varchar(15) DEFAULT '192.168.1.1',
  `localAreaNetwork4Mask` varchar(15) DEFAULT '255.255.255.0',
  `maxNumberOfReturnedSearchRecords` int NOT NULL DEFAULT '512',
  `maxNumberOfDisplayedRowsPerPage` int NOT NULL DEFAULT '64',
  `minAgeCriteriaForUsers` int NOT NULL DEFAULT '4',
  `applicationCounter` int NOT NULL DEFAULT '0',
  `revisionNumber` int NOT NULL DEFAULT '1',
  `revisionTime` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `version` varchar(8) NOT NULL DEFAULT '0',
  `tinNumber` varchar(16) DEFAULT NULL,
  `systemHash` char(32) DEFAULT NULL,
  `lockStatic` tinyint NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`profileId`),
  UNIQUE KEY `profileName` (`profileName`),
  UNIQUE KEY `systemName` (`systemName`),
  UNIQUE KEY `lockStatic` (`lockStatic`),
  KEY `countryId` (`countryId`),
  KEY `pHPTimezoneId` (`pHPTimezoneId`),
  KEY `firstDayOfAWeekId` (`firstDayOfAWeekId`),
  CONSTRAINT `_profile_ibfk_1` FOREIGN KEY (`countryId`) REFERENCES `_country` (`countryId`),
  CONSTRAINT `_profile_ibfk_2` FOREIGN KEY (`pHPTimezoneId`) REFERENCES `_pHPTimezone` (`zoneId`),
  CONSTRAINT `_profile_ibfk_3` FOREIGN KEY (`firstDayOfAWeekId`) REFERENCES `_daysOfAWeek` (`dayId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_profile`
--

LOCK TABLES `_profile` WRITE;
/*!40000 ALTER TABLE `_profile` DISABLE KEYS */;
INSERT INTO `_profile` VALUES (1,'0000:00:00:00:00:00','0000:00:00:00:00:00','Ndimangwa & Valentina','Geneology Workshop',NULL,NULL,NULL,NULL,NULL,NULL,'P.O Box 7436','Moshi','Kilimanjaro',1,1,'1983:05:06:23:07:57',1,'data/profile/profile.xml',NULL,'192.168.1.1','255.255.255.0',512,64,4,0,1,'0000:00:00:00:00:00','2.64',NULL,NULL,1,NULL,NULL,0);
/*!40000 ALTER TABLE `_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_securityQuestion`
--

DROP TABLE IF EXISTS `_securityQuestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_securityQuestion` (
  `questionId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `questionName` varchar(96) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`questionId`),
  UNIQUE KEY `questionName` (`questionName`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_securityQuestion`
--

LOCK TABLES `_securityQuestion` WRITE;
/*!40000 ALTER TABLE `_securityQuestion` DISABLE KEYS */;
INSERT INTO `_securityQuestion` VALUES (1,'0000:00:00:00:00:00','0000:00:00:00:00:00','What was your childhood nickname?',NULL,NULL,0),(2,'0000:00:00:00:00:00','0000:00:00:00:00:00','In what city did you meet your spouse/significant other?',NULL,NULL,0),(3,'0000:00:00:00:00:00','0000:00:00:00:00:00','What is the name of your favorite childhood friend?',NULL,NULL,0),(4,'0000:00:00:00:00:00','0000:00:00:00:00:00','What street did you live on in third grade?',NULL,NULL,0),(5,'0000:00:00:00:00:00','0000:00:00:00:00:00','What is your oldest sibling\'s birthday month and year? (e.g. January 1900)',NULL,NULL,0),(6,'0000:00:00:00:00:00','0000:00:00:00:00:00','What is the middle name of your oldest child?',NULL,NULL,0),(7,'0000:00:00:00:00:00','0000:00:00:00:00:00','What is your oldest sibling\'s middle name?',NULL,NULL,0),(8,'0000:00:00:00:00:00','0000:00:00:00:00:00','What school did you attend for sixth grade?',NULL,NULL,0),(9,'0000:00:00:00:00:00','0000:00:00:00:00:00','What was your childhood phone number including area code? (e.g. 000-000-0000)',NULL,NULL,0),(10,'0000:00:00:00:00:00','0000:00:00:00:00:00','What is your oldest cousin\'s first and last name?',NULL,NULL,0),(11,'0000:00:00:00:00:00','0000:00:00:00:00:00','What was the name of your first stuffed animal?',NULL,NULL,0),(12,'0000:00:00:00:00:00','0000:00:00:00:00:00','In what city or town did your mother and father meet?',NULL,NULL,0),(13,'0000:00:00:00:00:00','0000:00:00:00:00:00','Where were you when you had your first kiss?',NULL,NULL,0),(14,'0000:00:00:00:00:00','0000:00:00:00:00:00','What is the first name of the boy or girl that you first kissed?',NULL,NULL,0),(15,'0000:00:00:00:00:00','0000:00:00:00:00:00','What was the last name of your third grade teacher?',NULL,NULL,0),(16,'0000:00:00:00:00:00','0000:00:00:00:00:00','In what city does your nearest sibling live?',NULL,NULL,0),(17,'0000:00:00:00:00:00','0000:00:00:00:00:00','What is your oldest brother\'s birthday month and year? (e.g. January 1900)',NULL,NULL,0),(18,'0000:00:00:00:00:00','0000:00:00:00:00:00','What is your maternal grandmother\'s maiden name?',NULL,NULL,0),(19,'0000:00:00:00:00:00','0000:00:00:00:00:00','In what city or town was your first job?',NULL,NULL,0),(20,'0000:00:00:00:00:00','0000:00:00:00:00:00','What is the name of the place your wedding reception was held?',NULL,NULL,0),(21,'0000:00:00:00:00:00','0000:00:00:00:00:00','What is the name of a college you applied to but didn\'t attend?',NULL,NULL,0),(22,'0000:00:00:00:00:00','0000:00:00:00:00:00','Where were you when you first heard about 9/11?',NULL,NULL,0);
/*!40000 ALTER TABLE `_securityQuestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sex`
--

DROP TABLE IF EXISTS `_sex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_sex` (
  `sexId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `sexName` varchar(8) NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`sexId`),
  UNIQUE KEY `sexName` (`sexName`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sex`
--

LOCK TABLES `_sex` WRITE;
/*!40000 ALTER TABLE `_sex` DISABLE KEYS */;
INSERT INTO `_sex` VALUES (1,'0000:00:00:00:00:00','0000:00:00:00:00:00','Male',NULL,NULL,0),(2,'0000:00:00:00:00:00','0000:00:00:00:00:00','Female',NULL,NULL,0);
/*!40000 ALTER TABLE `_sex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_systemPolicy`
--

DROP TABLE IF EXISTS `_systemPolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_systemPolicy` (
  `policyId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `policyName` varchar(64) NOT NULL,
  `caption` varchar(96) DEFAULT NULL,
  `root` tinyint DEFAULT '0',
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`policyId`),
  UNIQUE KEY `policyName` (`policyName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_systemPolicy`
--

LOCK TABLES `_systemPolicy` WRITE;
/*!40000 ALTER TABLE `_systemPolicy` DISABLE KEYS */;
/*!40000 ALTER TABLE `_systemPolicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_systemUser`
--

DROP TABLE IF EXISTS `_systemUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_systemUser` (
  `userId` int unsigned NOT NULL AUTO_INCREMENT,
  `loginId` int unsigned NOT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`userId`),
  KEY `loginId` (`loginId`),
  CONSTRAINT `_systemUser_ibfk_1` FOREIGN KEY (`loginId`) REFERENCES `_login` (`loginId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_systemUser`
--

LOCK TABLES `_systemUser` WRITE;
/*!40000 ALTER TABLE `_systemUser` DISABLE KEYS */;
INSERT INTO `_systemUser` VALUES (1,1,NULL,'Ndimangwa Fadhili Ngoya',0),(2,2,NULL,'Valentina Nyamanoko Bwire',0);
/*!40000 ALTER TABLE `_systemUser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_systemlogs`
--

DROP TABLE IF EXISTS `_systemlogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_systemlogs` (
  `logId` int unsigned NOT NULL AUTO_INCREMENT,
  `logDate` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `username` varchar(56) NOT NULL,
  `contextPosition` int unsigned NOT NULL,
  `opname` varchar(56) NOT NULL,
  `target` varchar(96) NOT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`logId`),
  KEY `contextPosition` (`contextPosition`),
  CONSTRAINT `_systemlogs_ibfk_1` FOREIGN KEY (`contextPosition`) REFERENCES `_contextPosition` (`cId`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_systemlogs`
--

LOCK TABLES `_systemlogs` WRITE;
/*!40000 ALTER TABLE `_systemlogs` DISABLE KEYS */;
INSERT INTO `_systemlogs` VALUES (1,'2021:11:14:14:00:38','ndimangwa',44,'person_create','[ Seth Adam ] created',0),(2,'2021:11:14:14:02:11','ndimangwa',44,'person_create','[ Enos Seth Adam ] created',0),(3,'2021:11:14:14:03:32','ndimangwa',44,'person_create','[ Cainan Enos Seth ] created',0),(4,'2021:11:14:14:05:25','ndimangwa',44,'person_create','[ Mahalaleel Cainan Enos ] created',0),(5,'2021:11:14:14:06:53','ndimangwa',44,'person_create','[ Jared Mahalaleel Cainan ] created',0),(6,'2021:11:14:14:08:46','ndimangwa',44,'person_create','[ Enoch Jared Mahalaleel ] created',0),(7,'2021:11:14:14:10:11','ndimangwa',44,'person_create','[ Methuselah Enoch Jared ] created',0),(8,'2021:11:14:14:12:03','ndimangwa',44,'person_create','[ Lamech Methuselah Enoch ] created',0),(9,'2021:11:14:14:13:44','ndimangwa',44,'person_create','[ Noah Lamech Methuselah ] created',0),(10,'2021:11:14:14:14:27','ndimangwa',44,'person_create','[ Shem Noah Lamech ] created',0),(11,'2021:11:14:14:14:54','ndimangwa',44,'person_create','[ Ham Noah Lamech ] created',0),(12,'2021:11:14:14:15:22','ndimangwa',44,'person_create','[ Japeth Noah Lamech ] created',0),(13,'2021:11:14:14:28:15','ndimangwa',44,'person_create','[ Ndimangwa Noah Lamech ] created',0),(14,'2021:11:14:14:29:01','ndimangwa',46,'person_update','[ Ndimangwa Noah Lamech ] updated',0),(15,'2021:11:14:14:30:56','ndimangwa',46,'person_update','[ Ndimangwa Noah Lamech ] updated',0),(16,'2021:11:14:14:31:28','ndimangwa',47,'person_delete','Deleted Successful',0),(17,'2021:11:14:19:06:35','ndimangwa',1,'login','Logged-In into your account',0),(18,'2021:11:18:16:21:19','ndimangwa',1,'login','Logged-In into your account',0);
/*!40000 ALTER TABLE `_systemlogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_theme`
--

DROP TABLE IF EXISTS `_theme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_theme` (
  `themeId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `themeName` varchar(24) NOT NULL,
  `folder` varchar(16) DEFAULT NULL,
  `backgroundColour` varchar(6) DEFAULT NULL,
  `backgroundImage` varchar(16) DEFAULT NULL,
  `fontFace` varchar(108) DEFAULT NULL,
  `fontSize` varchar(8) DEFAULT NULL,
  `fontColour` varchar(6) DEFAULT NULL,
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`themeId`),
  UNIQUE KEY `themeName` (`themeName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_theme`
--

LOCK TABLES `_theme` WRITE;
/*!40000 ALTER TABLE `_theme` DISABLE KEYS */;
/*!40000 ALTER TABLE `_theme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_userStatus`
--

DROP TABLE IF EXISTS `_userStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_userStatus` (
  `statusId` int unsigned NOT NULL AUTO_INCREMENT,
  `timeOfCreation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `timeOfUpdation` varchar(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
  `statusName` varchar(32) NOT NULL,
  `code` int NOT NULL,
  `alive` tinyint NOT NULL DEFAULT '1',
  `extraFilter` varchar(32) DEFAULT NULL,
  `comments` varchar(64) DEFAULT NULL,
  `flags` int DEFAULT '0',
  PRIMARY KEY (`statusId`),
  UNIQUE KEY `statusName` (`statusName`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_userStatus`
--

LOCK TABLES `_userStatus` WRITE;
/*!40000 ALTER TABLE `_userStatus` DISABLE KEYS */;
INSERT INTO `_userStatus` VALUES (1,'0000:00:00:00:00:00','0000:00:00:00:00:00','Alive',1,1,NULL,NULL,0),(2,'0000:00:00:00:00:00','0000:00:00:00:00:00','Dead',2,0,NULL,NULL,0),(3,'0000:00:00:00:00:00','0000:00:00:00:00:00','Leave',3,1,NULL,NULL,0),(4,'0000:00:00:00:00:00','0000:00:00:00:00:00','Martenity',4,1,NULL,NULL,0),(5,'0000:00:00:00:00:00','0000:00:00:00:00:00','Partenity',5,1,NULL,NULL,0);
/*!40000 ALTER TABLE `_userStatus` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 16:26:02
