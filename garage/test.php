<?php 
include("../html/sys/__autoload__.php");
class Test {
    public static function createFormSwitchSelectInput($conn1, $classname, $name, $caption, $refObject1 = null, $isrequired = true, $validationColumnName = null, $fieldBlock1 = null)	{
		$otherproperties = ""; $title = "";
		if (isset($fieldBlock1['disabled'])) $otherproperties = " disabled";
		if (isset($fieldBlock1['title'])) {
			$ltitle = $fieldBlock1['title'];
			$title = " data-toggle = \"tooltip\" title = \"$ltitle\"";
			$otherproperties .= $title;
		}
		$checked = ""; if (isset($fieldBlock1['checked'])) $checked = "checked";
		$required = ""; if ($isrequired) $required = "required";
		if (is_null($validationColumnName)) $validationColumnName = $name;
		$validationRule = Registry::getUIControlValidations($classname, $validationColumnName, "select"); //All Inputs are validated as text , unless otherwise, select control not included
		$window1 = "<div $title class=\"form-group row\">";
		$switchId = "__switch_select_$name"; 
		//$window1 .= "<label $title for=\"$name\" class=\"col-sm-2 col-form-label\">$caption</label>";
		$window1 .= "<div class=\"col-sm-2\"><input id=\"$switchId\" type=\"checkbox\"  $checked data-bootstrap-switch data-off-color=\"danger\" data-on-color=\"success\"/></div>";
		$window1 .= "<div class=\"col-sm-2\"><label for=\"$name\">$caption</label></div>";
		$defaultValue = Constant::$default_select_empty_value;
		$window1 .= "<div class=\"col-sm-8\"><select $otherproperties class=\"form-control\" id=\"$name\" name=\"$name\" $required $validationRule><option value=\"$defaultValue\">(-- Select --)</option>";
		if (is_null($validationColumnName)) $validationColumnName = $name;
		$refClassname = Registry::getReferenceClass($classname, $validationColumnName);
		$dataArray1 = null; if (! is_null($refClassname)) $dataArray1 = Registry::loadAllData($conn1, $refClassname);
		if (! is_null($dataArray1))	{
			foreach ($dataArray1 as $dataBlock1)	{
				$dvalue = $dataBlock1['__id__'];
				$dcaption = $dataBlock1['__name__'];
				$selected = "";  if (! is_null($refObject1) && ($dvalue == $refObject1->getId0())) $selected = "selected";
				$window1 .= "<option $selected value=\"$dvalue\">$dcaption</option>";
			}
		}
		$window1 .= "</select></div>";
		$window1 .= "</div>";  //Close .form-group.row
		$window1 .= "<script type=\"text/javascript\">    $('#$switchId').on('change.bootstrapSwitch', function(e) {       $('#$name').prop('disabled', ! e.target.checked);      });</script>";
		return $window1;
	}
}
?>