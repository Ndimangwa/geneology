<?php 
require_once("../html/common/__autoload__.php");
require_once("../html/sys/__autoload__.php");
$config1 = new ConfigurationData("../html/config.php");
$dbname = $config1->getDatabase();
$host = $config1->getHostname();
$conn = null;
try {
    $conn = new PDO("mysql:dbname=$dbname;host=$host", $config1->getUsername(), $config1->getPassword());
    $profile1 = new Profile($dbname, __data__::$__PROFILE_INIT_ID, $conn);
    date_default_timezone_set($profile1->getPHPTimezone()->getZoneName());
    $systemTime1 = new DateAndTime(date("Y:m:d:H:i:s"));
    $login1 = new Login($dbname, __data__::$__LOGIN_INIT_ID, $conn);
    /*echo Finance::raiseInvoice(
        $conn,
        $login1,
        $systemTime1,
        "JobTitle.1",
        array(
            array('serviceId' => 67),
            array('amount' => 900000),
            array('serviceId' => 68, 'amount' => 250000)
        ),
        Currency::$__LOCAL_CURRENCY,
        "Kwangu Tu",
        600,
        "Group.789",
        "https://google.com/ndimangwa/id=71"
    );*/
    echo (Finance::raiseInvoice(
        $conn,
        $login1,
        $systemTime1,
        "JobTitle.1",
        array(
            array('serviceId' => 67),
            array('amount' => 900000),
            array('serviceId' => 68, 'amount' => 250000)
        ),
        Currency::$__LOCAL_CURRENCY,
        "Kwangu Tu",
        null,
        600,
        "Group.789",
        "https://google.doodle.ui",
        null,
        8979,
        200
    ))->getInvoiceId();

} catch (Exception $e)  {
    die($e->getMessage());
}
$conn = null;
