<?php 
require_once("../html/common/__autoload__.php");
require_once("../html/sys/__autoload__.php");
$config1 = new ConfigurationData("../html/config.php");
$dbname = $config1->getDatabase();
$host = $config1->getHostname();
$conn = null;
$amount = 500000;
$invoiceId = 1;
$payerName = null;
$payerPhone = null;
try {
    $conn = new PDO("mysql:dbname=$dbname;host=$host", $config1->getUsername(), $config1->getPassword());
    $profile1 = new Profile($dbname, __data__::$__PROFILE_INIT_ID, $conn);
    date_default_timezone_set($profile1->getPHPTimezone()->getZoneName());
    $systemTime1 = new DateAndTime(date("Y:m:d:H:i:s"));
    $login1 = new Login($dbname, __data__::$__LOGIN_INIT_ID, $conn);
    echo Finance::issueReceipt(
        $conn,
        $login1,
        $systemTime1,
        $invoiceId,
        $amount,
        "Group.909",
        "https://receivergateway.org",
        $payerName,
        $payerPhone
    );
} catch (Exception $e)  {
    die($e->getMessage());
}
$conn = null;
