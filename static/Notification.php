<?php 
class Notification {
    public static function createNotification($conn, $systemTime1, $caption, $targetReference, $categoryId, $forwardURL = null, $numberOfValidDays = 100) {
        $dataArray1 = array(
            "timeOfCreation" => $systemTime1->getDateAndTimeString(),
            "timeOfUpdation" => $systemTime1->getDateAndTimeString(),
            "caption" => $caption,
            "numberOfValidDays" => $numberOfValidDays,
            "targetReference" => $targetReference,
            "category" => $categoryId,
            "closed" => 0
        );
        if (! is_null($forwardURL)) $dataArray1['forwardURL'] = $forwardURL;
       return __data__::insert($conn, "Notification", $dataArray1);
    }
}
?>