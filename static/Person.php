<?php 
class Person    {
    public function getFullName($ancestorLevel = 2)   {
        $person1 = $this;
        $myname = $person1->getPersonName();
        for ($i = 0; $i < $ancestorLevel; $i++) {
            $person1 = ! is_null($person1->getFather()) ? $person1->getFather() : $person1->getMother();
            if (is_null($person1)) break;
            $myname .= " ".$person1->getPersonName();
        }
        return $myname;
    }
    public function getListOfPeopleWhoWereAliveDuringMyLifetime()  {
        $me_a = $this->getYearOfBirth();
        $me_b = $this->getYearOfDeath(); //check for zero
        $personId = $this->personId;
        $query = "SELECT personId FROM _person as p WHERE personId <> '$personId'";
        $records = __data__::getSelectedRecords($this->conn, $query, false);
        $listOfPeople = array();
        foreach ($records['column'] as $bdata)  {
            $person1 = new Person("Delta", $bdata['personId'], $this->conn);
            $you_a = $person1->getYearOfBirth();
            $you_b = $person1->getYearOfDeath();
            if (/*$me_b == 0 || */$you_b == 0) continue;
            if (( $me_b != 0 && $you_a > $me_b) || $me_a > $you_b) continue;
            $listOfPeople[sizeof($listOfPeople)] = $person1;
        }
        if (sizeof($listOfPeople) == 0) $listOfPeople = null;
        return $listOfPeople;
    }
    public static function getGeneologyTable($conn) {
       $window1 = "<div class=\"table-responsive\"><table class=\"table\"><thead><tr><th scope=\"col\"></th><th scope=\"col\">Name</th><th scope=\"col\">Birth Year</th><th scope=\"col\">Death Year</th><th scope=\"col\">Who were presentM<br/>During Lifetime</th></tr></thead><tbody>";
       $query = "SELECT personId FROM _person ORDER BY personId ASC";
       $records = __data__::getSelectedRecords($conn, $query, false);
       $count = 0;
       foreach ($records['column'] as $bdata)   {
            $person1 = new Person("Hello", $bdata['personId'], $conn);
            $personName = $person1->getFullName(1);
            $count++;
            $yearOfBirth = $person1->getYearOfBirth();
            $yearOfDeath = $person1->getYearOfDeath();
            if ($yearOfDeath == 0) $yearOfDeath = "---";
            $window1 .= "<tr><th scope=\"row\">$count</th><td>$personName</td><td>$yearOfBirth</td><td>$yearOfDeath</td><td style=\"font-size: 0.8em;\">";
            $listOfPeople = $person1->getListOfPeopleWhoWereAliveDuringMyLifetime();
            if (! is_null($listOfPeople))   {
                $index = 0;
                foreach ($listOfPeople as $person1) {
                    $personName = $person1->getPersonName();
                    $t1 = "<span>$personName</span>";
                    if ($index == 0) $window1 .= $t1;
                    else $window1 .= ", $t1";
                    $index++;
                }
            }
            $window1 .= "</td></tr>";
       }
       $window1 .= "</tbody></table></div>";
       return $window1;
    }
}
?>