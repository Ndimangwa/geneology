<?php 
class Hospital {
    public static function generateRegistrationNumber($conn, $rollback = true)    {
        //Once Issue the pointer has to advance
        $hospital1 = new Hospital("Ndimangwa", Hospital::$__INIT_ID, $conn);
        $regNumber = $hospital1->getRegistrationNumberPrefix();
        $tnumber = __object__::fixLength($hospital1->getNextRegistrationNumber(), $hospital1->getRegistrationNumberWidth(), "0");
        $regNumber .= $tnumber;
        $hospital1->setNextRegistrationNumber($hospital1->getNextRegistrationNumber() + 1);
        $hospital1->update($rollback);
        return $regNumber;
    }
    public static function generateInvoiceNumber($conn, $rollback = true)    {
        //Once Issue the pointer has to advance
        $hospital1 = new Hospital("Ndimangwa", Hospital::$__INIT_ID, $conn);
        $regNumber = $hospital1->getInvoiceNumberPrefix();
        $tnumber = __object__::fixLength($hospital1->getNextInvoiceNumber(), $hospital1->getInvoiceNumberWidth(), "0");
        $regNumber .= $tnumber;
        $hospital1->setNextInvoiceNumber($hospital1->getNextInvoiceNumber() + 1);
        $hospital1->update($rollback);
        return $regNumber;
    }
    public static function generateReceiptNumber($conn, $rollback = true)    {
        //Once Issue the pointer has to advance
        $hospital1 = new Hospital("Ndimangwa", Hospital::$__INIT_ID, $conn);
        $regNumber = $hospital1->getReceiptNumberPrefix();
        $tnumber = __object__::fixLength($hospital1->getNextReceiptNumber(), $hospital1->getReceiptNumberWidth(), "0");
        $regNumber .= $tnumber;
        $hospital1->setNextReceiptNumber($hospital1->getNextReceiptNumber() + 1);
        $hospital1->update($rollback);
        return $regNumber;
    }
}
?>