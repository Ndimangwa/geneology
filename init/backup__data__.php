<?php 
    /*
Developed by Ndimangwa Fadhili Ngoya
Developed on 15th April, 2021
Phone: +255 787 101 808 / +255 762 357 596
Email: ndimangwa@gmail.com 

Initialization codes for data related objects
*/
abstract class __data__ extends __object__ {
	public static $__PROFILE_INIT_ID = 1;
	public static $__LOGIN_INIT_ID = 1;
	public static $__USER_INIT_ID = 1;
	private $l__update = array(); //used to keep track of updates
	private $p_l_update = array(); //Keep property updates
	public static $__ALL32BITS_SET = 4294967295;
	//ReferenceString 
	public function isMemberOfObjectReference($referenceString)	{
		if (is_null($referenceString)) return false;
		$classname = $this->getMyClassname();
		if (! in_array($classname, array('Login', 'JobTitle', 'Group'))) return false;
		if ($classname == "Login") {
			return ($this->getObjectReferenceString() == $referenceString || $this->getJobTitle()->isMemberOfObjectReference($referenceString) || $this->getGroup()->isMemberOfObjectReference($referenceString));
		} else if ($classname == "JobTitle")	{
			return ($this->getObjectReferenceString() == $referenceString);
		} else if ($classname == "Group")	{
			$pgroup1 = $this->getParentGroup();
			return (($this->getObjectReferenceString() == $referenceString) || (! is_null($pgroup1) && $pgroup1->isMemberOfObjectReference($referenceString)));
		}
		return false;
	}
	//Working with Data
	public static function convertRawSQLDataToTabularData($conn, $classname, $rows)	{
		$tArray1 = array();
		$primaryColumn = Registry::getId0Columnname($classname);
		foreach ($rows as $row)	{
			$index = sizeof($tArray1);
			$tArray1[$index] = array();
			foreach ($row as $colname => $value)	{
				$pname = Registry::column2Property($classname, $colname);
				if (is_null($pname)) continue;
				if ($colname == $primaryColumn) $pname = "id";
				//We need to work with Dates 
				$refclass = Registry::getReferenceClass($classname, $pname);
				if ($refclass == "DateAndTime")	{
					try { $value = DateAndTime::convertFromSystemDateAndTimeFormatToGUIDateFormat($value); } catch (Exception $e)	{}
				}
				//We need to work for value 
				$tmap = Registry::columnTransitiveMap($classname, $pname);
				$newValue = null;
				if (! is_null($tmap) && is_array($tmap))	{
					foreach ($tmap as $tclassproperty)	{
						$tval = self::getValueOfAClassProperty($conn, $tclassproperty, $value);
						if (! is_null($tval))	{
							if (is_null($newValue)) $newValue = $tval;
						} else {
							$newValue .= " -- $tval";
						}
					}
				}
				if (is_null($newValue)) $newValue = $value;
				$tArray1[$index][$pname] = $newValue;
			}
		}
		return $tArray1;
	}
	public static function getValueOfAClassProperty($conn, $classproperty /* ie Sex.sexName */, $pid /*  ie 2*/)	{
		// return value of a property of a class given a primary value 
		if (is_null($classproperty)) return null;
		$tArray1 = explode(".", $classproperty);
		if (sizeof($tArray1) != 2) return null;
		$classname = $tArray1[0];
		$pname = $tArray1[1];
		$primaryColumn = Registry::getId0Columnname($classname);
		$colname = Registry::property2column($classname, $pname);
		if (is_null($primaryColumn)) return null;
		$jresult1 = SQLEngine::execute(SimpleQueryBuilder::buildSelect(
			array(Registry::getTablename($classname)),
			array($colname),
			array($primaryColumn => $pid)
		), $conn);
		$jArray1 = json_decode($jresult1, true);
		if (is_null($jArray1)) return null;
		if ($jArray1['code'] !== 0) return null;
		if ($jArray1['count'] !== 1) return null; //Must be one , since we submited pid -- unique 
		return $jArray1['rows'][0][$colname];
	}
	//Alerts And Warnings 
	private static function getAlert($message, $alertType)	{
		return "<div class=\"alert $alertType\" role=\"alert\">$message</div>";
	}
	public static function showPrimaryAlert($message)	{
		return self::getAlert($message, "alert-primary");
	}
	public static function showDangerAlert($message)	{
		return self::getAlert($message, "alert-danger");
	}
	public static function showWarningAlert($message)	{
		return self::getAlert($message, "alert-warning");
	}
	//FormControls 
	public static function createDetailsPage($page, $classname, $payload /* 1D columnlist */, $conn, $id, $extraControls = null, $appendData = array())	{
		if (is_null($appendData)) $appendData = array(); /*colname => data*/
		$line = "<div class=\"data_details\"><table class=\"table\"><thead class=\"thead-dark\"><th scope=\"col\"></th><th>Name</th><th>Value</th></thead><tbody>";
		$tablename = Registry::getTablename($classname);
		$primaryColumn = Registry::getId0Columnname($classname);
		$whereArray1 = array();
		$whereArray1[$primaryColumn] = $id;
		$listOfColumns = array();
		foreach ($payload as $pname)	{
			$dt = Registry::property2Column($classname, $pname);
			if (! is_null($dt))	{
				$listOfColumns[sizeof($listOfColumns)] = $dt;
			}
		}
		$jresult1 = SQLEngine::execute(SimpleQueryBuilder::buildSelect(
			array($tablename),
			$listOfColumns,
			$whereArray1
		), $conn);
		if (is_null($jresult1)) throw new Exception("Results Returned null");
		$jArray1 = json_decode($jresult1, true);
		if (is_null($jArray1)) throw new Exception("Malformed Return Value");
		if ($jArray1['code'] != 0) throw new Exception($jArray1['message']);
		if ($jArray1['count'] != 1) throw new Exception("Empty, or Duplicate records");
		$rows = self::convertRawSQLDataToTabularData($conn, $classname, $jArray1['rows']);
		$row = $rows[0];
		$count = 0;
		foreach ($listOfColumns as $column)	{
			$pname = Registry::column2property($classname, $column);
			if (! is_null($pname))	{
				$sn = $count + 1;
				$caption = __object__::property2Caption($pname);
				$value = $row[$pname];
				if (is_null($value)) continue;
				if ($value == "") continue;
				//We need to do translation
				if (isset($appendData[$pname])) $value .= $appendData[$pname];
				$line .= "<tr><th scope=\"row\">$sn</th><td>$caption</td><td>$value</td></tr>";
				$count++;
			}
		}
		$line .= "</tbody></table>";
		//Working with further controls 
		if (! is_null($extraControls))	{
			$line .= "<div class=\"extra-controls-container\" class=\"mt-2 mr-2 mb-2 ml-auto text-right\">";
			$count = 0;
			foreach ($extraControls as $controlBlock1)	{
				if (! (isset($controlBlock1['pname']) && isset($controlBlock1['href']))) continue;
				$pname = $controlBlock1['pname'];
				$href = $controlBlock1['href'];
				$caption = null; if (isset($controlBlock1['caption'])) $caption = $controlBlock1['caption'];
				$policy = null; if (isset($controlBlock1['policy'])) $policy = $controlBlock1['policy'];
				$title = null; if (isset($controlBlock1['title']))	$title = $controlBlock1['title'];
				$icon = null; if (isset($controlBlock1['icon-class'])) $icon = $controlBlock1['icon-class'];
				if (is_null($policy) || $policy)	{
					
					$m_left = "ml-2"; if ($count == 0) $m_left = "";
					if (! is_null($title)) $title = "title = \"$title\""; else $title = "";
					if (is_null($caption)) $caption = ""; 
					if (! is_null($icon)) $icon = "<i class=\"$icon\"></i>";else $icon = "";
					$line .= "<a class=\"cmd cmd_$pname $m_left\" href=\"$href\" data-id=\"$id\" data-class=\"$classname\" data-toggle=\"tooltip\" $title>$icon $caption</a>";
					$count++;
				}
			}
			$line .= "</div>";
		}
		$line .= "</div>";
		return $line;
	}
	public static function createConfirmationForm($page, $classname, $message, $buttonText = "Confirm", $mode = "delete", $conn = null, $id = -1, $customHiddenFields = null, $customContextName = null)	{
		$formid = __object__::getCodeString(16);
		$errorid = "__form_error_$formid";
		$formid = "__form_reference_$formid";
		$line = "<div><form class=\"form-horizontal\" id=\"$formid\" method=\"POST\">";
		$line .= "<input type=\"hidden\" name=\"__classname__\" value=\"$classname\"/>";
		if (! is_null($customContextName)) $line .= "<input type=\"hidden\" name=\"__custom_context_name__\" value=\"$customContextName\"/>";
		$line .= "<input type=\"hidden\" name=\"__query__\" value=\"$mode\"/>";
		if (! is_null($customHiddenFields))	{
			foreach ($customHiddenFields as $keyname => $val)	{
				$line .= "<input type=\"hidden\" name=\"$keyname\" value=\"$val\"/>";
			}
		}
		$line .= "<div><label>$message</label></div><div id=\"$errorid\" class=\"p-2 ui-sys-error-message\"></div>";
		$line .= "<div><button class=\"btn btn-danger btn-block btn-click-default btn-execute-on-click btn-send-dialog-ajax\" type=\"button\" data-form-submit=\"$formid\" data-next-page=\"$page\" data-form-error=\"$errorid\">$buttonText</button></div>";
		$line .= "</form></div>";
		return $line;
	}
	public static function createMutualExclusiveControls($conn, $classname, $caption, $fieldBlock1, $fieldBlock2, $checked = true, $update = false, $class_id = null)	{
		$object1 = null; if ($update) $object1 = Registry::getObjectReference("Delta", $conn, $classname, $class_id);
		$compulsoryFields = array('pname');
		foreach ($compulsoryFields as $field) if (! isset($fieldBlock1[$field]) && ! isset($fieldBlock2[$field])) throw new Exception("[ $field ] : Not set in either block1 or block2");
		//pname 
		$pname1 = $fieldBlock1['pname']; $pname2 = $fieldBlock2['pname'];		
		//caption
		$caption1 = null; if (isset($fieldBlock1['caption'])) $caption1 = $fieldBlock1['caption']; else $caption1 = __object__::property2Caption($pname1);
		$caption2 = null; if (isset($fieldBlock2['caption'])) $caption2 = $fieldBlock2['caption']; else $caption2 = __object__::property2Caption($pname2);
		//required
		$required1 = true; if (isset($fieldBlock1['required'])) $required1 = $fieldBlock1['required'];
		$required2 = true; if (isset($fieldBlock2['required'])) $required2 = $fieldBlock2['required'];
		//placeholder 
		$placeholder1 = null; if (isset($fieldBlock1['placeholder'])) $placeholder1 = $fieldBlock1['placeholder'];
		$placeholder2 = null; if (isset($fieldBlock2['placeholder'])) $placeholder2 = $fieldBlock2['placeholder'];
		//validationColumnName
		$validationColumnName1 = null; if (isset($fieldBlock1['validation-column-name'])) $validationColumnName1 = $fieldBlock1['validation-column-name'];
		$validationColumnName2 = null; if (isset($fieldBlock2['validation-column-name'])) $validationColumnName2 = $fieldBlock2['validation-column-name'];
		//uiControl
		$uiRenderControl1 = null; if (isset($fieldBlock1['ui-control'])) $uiRenderControl1 = $fieldBlock1['ui-control'];
		$uiRenderControl2 = null; if (isset($fieldBlock2['ui-control'])) $uiRenderControl2 = $fieldBlock2['ui-control'];
		//We need to work with type 
		$type1 = Registry::getColumnType($classname, $pname1);
		$type2 = Registry::getColumnType($classname, $pname2);
		if (isset($fieldBlock1['type'])) $type1 = $fieldBlock1['type'];
		if (isset($fieldBlock2['type'])) $type2 = $fieldBlock2['type'];
		if (is_null($type1) || is_null($type2)) throw new Exception("One of the types, could not be figured");
		//propertyValue 
		$propertyValue1 = null; $propertyValue2 = null;
		if (! is_null($object1)) {
			$propertyValue1 = $object1->getMyPropertyValue($pname1);
			$propertyValue2 = $object1->getMyPropertyValue($pname2);
		}
		if (isset($fieldBlock1['value'])) 	$propertyValue1 = $fieldBlock1['value'];
		if (isset($fieldBlock2['value'])) 	$propertyValue2 = $fieldBlock2['value'];
		//Checked Status 
		$checkedstatus = ""; 
		if ($checked) {
			$checkedstatus = "checked";
			$fieldBlock2['disabled'] = true;
		} else {
			$fieldBlock1['disabled'] = true;
		}
		$line = "";
		//1st Control
		if ($type1 == "object")	{
			$line .= self::createFormSelectInput($conn, $classname, $pname1, $caption1, $propertyValue1, $required1, $validationColumnName1, $fieldBlock1);
		} else if ($type1 == "text")	{
			$line .= self::createFormTextInput($classname, $pname1, $caption1, $propertyValue1, $required1, $validationColumnName1, $placeholder1, $fieldBlock1);
		} else if ($type1 == "integer")	{
			$line .= self::createFormNumberInput($classname, $pname1, $caption1, $propertyValue1, $required1, $validationColumnName1, $placeholder1, $fieldBlock1);
		} else if ($type1 == "float")	{
			$line .= self::createFormNumberInput($classname, $pname1, $caption1, $propertyValue1, $required1, $validationColumnName1, $placeholder1, $fieldBlock1);
		} else if ($type1 == "email")	{
			$line .= self::createFormEmailInput($classname, $pname1, $caption1, $propertyValue1, $required1, $validationColumnName1, $placeholder1, $fieldBlock1);
		} else if ($type1 == "date")	{
			$line .= self::createFormDateInput($classname, $pname1, $caption1, $propertyValue1, $required1, $validationColumnName1, $placeholder1, $fieldBlock1);
		} else if ($type1 == "switch-text")	{
			$line .= self::createFormSwitchTextInput($classname, $pname1, $caption1, $propertyValue1, $required1, $validationColumnName1, $placeholder1, $fieldBlock1);
		}
		//2nd Control
		if ($type2 == "object")	{
			$line .= self::createFormSelectInput($conn, $classname, $pname2, $caption2, $propertyValue2, $required2, $validationColumnName2, $fieldBlock2);
		} else if ($type2 == "text")	{
			$line .= self::createFormTextInput($classname, $pname2, $caption2, $propertyValue2, $required2, $validationColumnName2, $placeholder2, $fieldBlock2);
		} else if ($type2 == "integer")	{
			$line .= self::createFormNumberInput($classname, $pname2, $caption2, $propertyValue2, $required2, $validationColumnName2, $placeholder2, $fieldBlock2);
		} else if ($type2 == "float")	{
			$line .= self::createFormNumberInput($classname, $pname2, $caption2, $propertyValue2, $required2, $validationColumnName2, $placeholder2, $fieldBlock2);
		} else if ($type2 == "email")	{
			$line .= self::createFormEmailInput($classname, $pname2, $caption2, $propertyValue2, $required2, $validationColumnName2, $placeholder2, $fieldBlock2);
		} else if ($type2 == "date")	{
			$line .= self::createFormDateInput($classname, $pname2, $caption2, $propertyValue2, $required2, $validationColumnName2, $placeholder2, $fieldBlock2);
		} else if ($type2 == "switch-text")	{
			$line .= self::createFormSwitchTextInput($classname, $pname2, $caption2, $propertyValue2, $required2, $validationColumnName2, $placeholder2, $fieldBlock2);
		}
		$window1 = $line;
		//Building a switch ui
		$switchId = __object__::getCodeString(16);
		$line = "<div class=\"form-group row\"><div class=\"col-sm-2\"><input id=\"$switchId\" type=\"checkbox\" $checkedstatus data-bootstrap-switch data-off-color=\"danger\" data-on-color=\"success\"/></div><label class=\"col-sm-10 col-form-label\">$caption</label></div>";
		$window1 = $line.$window1;
		$window1 .= "<script type=\"text/javascript\">    function updateUI(checked)  {        var \$control1 = $(\"#$pname1\");        var \$control2 = $(\"#$pname2\");        if (! (\$control1.length && \$control2.length)) return false;        \$control1.prop('disabled', ! checked);        \$control2.prop('disabled', checked);    }    $('#$switchId').on('change.bootstrapSwitch', function(e)    {        updateUI(e.target.checked);    })</script>";
		return "<div class=\"border border-outline-primary my-2\">$window1</div>";
	}
	public static function createDataCaptureForm($page, $classname, $payload /*2D array*/, $buttonText = "Submit Data", $mode = "create", $conn = null, $id = -1, $customHiddenFields = null /*key value*/, $customContextName = null, $serverScript = null, $customDataSubmissionClass = "btn-send-dialog-ajax")	{
		$formid = __object__::getCodeString(16);
		$errorid = "__form_error_$formid";
		$formid = "__form_reference_$formid";
		$line = "<div><form class=\"form-horizontal\" method=\"POST\" id=\"$formid\">";
		//Hidden Fields
		$line .= "<input type=\"hidden\" name=\"__classname__\" value=\"$classname\"/>";
		if (! is_null($customContextName)) $line .= "<input type=\"hidden\" name=\"__custom_context_name__\" value=\"$customContextName\"/>";
		$line .= "<input type=\"hidden\" name=\"__query__\" value=\"$mode\"/>";
		if (! is_null($customHiddenFields))	{
			foreach ($customHiddenFields as $keyname => $val)	{
				$line .= "<input type=\"hidden\" name=\"$keyname\" value=\"$val\"/>";
			}
		}
		//Ui Control Fields
		$object1 = null;
		if ($mode == "update") $object1 = Registry::getObjectReference("Ndimangwa-Ngoya", $conn, $classname, $id);
		foreach ($payload as $fieldBlock1)	{
			if (! isset($fieldBlock1['pname'])) continue;
			$pname = $fieldBlock1['pname'];
			$caption = null;
			if (isset($fieldBlock1['caption'])) $caption = $fieldBlock1['caption'];
			else $caption = __object__::property2Caption($pname);
			$required = true; if (isset($fieldBlock1['required'])) $required = $fieldBlock1['required'];
			$placeholder = null; if (isset($fieldBlock1['placeholder'])) $placeholder = $fieldBlock1['placeholder'];
			$validationColumnName = null; if (isset($fieldBlock1['validation-column-name'])) $validationColumnName = $fieldBlock1['validation-column-name'];
			$uiRenderControl = null; if (isset($fieldBlock1['ui-control'])) $uiRenderControl = $fieldBlock1['ui-control'];
			//We need to Get Control Type 
			$type = Registry::getColumnType($classname, $pname);
			if (isset($fieldBlock1['type'])) $type = $fieldBlock1['type'];
			if (is_null($type)) continue;
			$propertyValue = null; if (! is_null($object1)) $propertyValue = $object1->getMyPropertyValue($pname);
			if (isset($fieldBlock1['value'])) $propertyValue = $fieldBlock1['value'];
			if ($type == "object")	{
				$line .= self::createFormSelectInput($conn, $classname, $pname, $caption, $propertyValue, $required, $validationColumnName, $fieldBlock1);
			} else if ($type == "text")	{
				$line .= self::createFormTextInput($classname, $pname, $caption, $propertyValue, $required, $validationColumnName, $placeholder, $fieldBlock1);
			} else if ($type == "integer")	{
				$line .= self::createFormNumberInput($classname, $pname, $caption, $propertyValue, $required, $validationColumnName, $placeholder, $fieldBlock1);
			} else if ($type == "float")	{
				$line .= self::createFormNumberInput($classname, $pname, $caption, $propertyValue, $required, $validationColumnName, $placeholder, $fieldBlock1);
			} else if ($type == "email")	{
				$line .= self::createFormEmailInput($classname, $pname, $caption, $propertyValue, $required, $validationColumnName, $placeholder, $fieldBlock1);
			} else if ($type == "date")	{
				$line .= self::createFormDateInput($classname, $pname, $caption, $propertyValue, $required, $validationColumnName, $placeholder, $fieldBlock1);
			} else if ($type == "switch-text")	{
				$line .= self::createFormSwitchTextInput($classname, $pname, $caption, $propertyValue, $required, $validationColumnName, $placeholder, $fieldBlock1);
			}
			//echo "\n[ type = $type ] ; ==== [ pname = $pname ]";
		}
		//Ui Error Field
		$line .= "<div id=\"$errorid\" class=\"p-2 ui-sys-error-message\"></div>";
		$dataServerScript = ""; if (! is_null($serverScript)) $dataServerScript = "data-server-script = \"$serverScript\"";
		$btnSendWithAjaxClass = "btn-send-dialog-ajax";
		$line .= "<div><button $dataServerScript class=\"btn btn-primary btn-block btn-click-default btn-execute-on-click $customDataSubmissionClass\" type=\"button\" data-form-submit=\"$formid\" data-next-page=\"$page\" data-form-error=\"$errorid\">$buttonText</button></div>";
		$line .= "</form></div>";
		return $line;
	}
	public static function createFormSelectInput($conn1, $classname, $name, $caption, $refObject1 = null, $isrequired = true, $validationColumnName = null, $fieldBlock1 = null)	{
		$otherproperties = ""; $title = "";
		if (isset($fieldBlock1['disabled'])) $otherproperties = " disabled";
		if (isset($fieldBlock1['title'])) {
			$ltitle = $fieldBlock1['title'];
			$title = " data-toggle = \"tooltip\" title = \"$ltitle\"";
			$otherproperties .= $title;
		}
		$required = ""; if ($isrequired) $required = "required";
		$validationRule = Registry::getUIControlValidations($classname, $name, "select"); //All Inputs are validated as text , unless otherwise, select control not included
		$defaultValue = Constant::$default_select_empty_value;
		$control1 =  "<div class=\"form-group row\"><label $title for=\"$name\" class=\"col-sm-2 col-form-label\">$caption</label><div class=\"col-sm-10\"><select $otherproperties class=\"form-control\" id=\"$name\" name=\"$name\" $required $validationRule><option value=\"$defaultValue\">(-- Select --)</option>";
		if (is_null($validationColumnName)) $validationColumnName = $name;
		$refClassname = Registry::getReferenceClass($classname, $validationColumnName);
		$dataArray1 = null; if (! is_null($refClassname)) $dataArray1 = Registry::loadAllData($conn1, $refClassname);
		if (! is_null($dataArray1))	{
			foreach ($dataArray1 as $dataBlock1)	{
				$dvalue = $dataBlock1['__id__'];
				$dcaption = $dataBlock1['__name__'];
				$selected = "";  if (! is_null($refObject1) && ($dvalue == $refObject1->getId0())) $selected = "selected";
				$control1 .= "<option $selected value=\"$dvalue\">$dcaption</option>";
			}
		}
		$control1 .= "</select></div></div>";
		return $control1;
	}
	public static function createFormPasswordInput($classname, $name, $caption, $value = null, $isrequired = true, $validationColumnName = null, $placeholder = null, $fieldBlock1 = null)	{
		return self::createGeneralFormInput($classname, $name, $caption, $value, $isrequired, "password", $validationColumnName, $placeholder, $fieldBlock1);
	}
	public static function createFormNumberInput($classname, $name, $caption, $value = null, $isrequired = true, $validationColumnName = null, $placeholder = null, $fieldBlock1 = null)	{
		return self::createGeneralFormInput($classname, $name, $caption, $value, $isrequired, "number", $validationColumnName, $placeholder, $fieldBlock1);
	}
	public static function createFormEmailInput($classname, $name, $caption, $value = null, $isrequired = true, $validationColumnName = null, $placeholder = null, $fieldBlock1 = null)	{
		return self::createGeneralFormInput($classname, $name, $caption, $value, $isrequired, "email", $validationColumnName, $placeholder, $fieldBlock1);
	}
	public static function createFormTextInput($classname, $name, $caption, $value = null, $isrequired = true, $validationColumnName = null, $placeholder = null, $fieldBlock1 = null)	{
		return self::createGeneralFormInput($classname, $name, $caption, $value, $isrequired, "text", $validationColumnName, $placeholder, $fieldBlock1);
	}
	public static function createFormSwitchTextInput($classname, $name, $caption, $value = null, $isrequired = true, $validationColumnName = null, $placeholder = null, $fieldBlock1 = null)	{
		$otherproperties = ""; $title = "";
		if (isset($fieldBlock1['disabled'])) $otherproperties = " disabled";
		if (isset($fieldBlock1['title'])) {
			$ltitle = $fieldBlock1['title'];
			$title = " data-toggle = \"tooltip\" title = \"$ltitle\"";
			$otherproperties .= $title;
		}
		$checked = ""; if (isset($fieldBlock1['checked'])) $checked = "checked";
		$required = ""; if ($isrequired) $required = "required";
		$fvalue = ""; if (! is_null($value)) $fvalue = "value = \"$value\"";
		$fplaceholder = ""; if (! is_null($placeholder)) $fplaceholder = "placeholder = \"$placeholder\"";
		if (is_null($validationColumnName)) $validationColumnName = $name;
		$validationRule = Registry::getUIControlValidations($classname, $validationColumnName, "text"); //All Inputs are validated as text , unless otherwise, select control not included
		$window1 = "<div class=\"form-group row\">";
		$switchId = "__switch_text_$name";
		//$window1 .= "<label $title for=\"$name\" class=\"col-sm-2 col-form-label\">$caption</label>";
		$window1 .= "<div $title class=\"col-sm-2\"><input id=\"$switchId\" type=\"checkbox\"  $checked data-bootstrap-switch data-off-color=\"danger\" data-on-color=\"success\"/></div>";
		$window1 .= "<div class=\"col-sm-10\"><input $otherproperties class=\"form-control\" id=\"$name\" name=\"$name\" type=\"text\" $fvalue $required $fplaceholder $validationRule /></div></div>";
		$window1 .= "<script type=\"text/javascript\">    $('#$switchId').on('change.bootstrapSwitch', function(e) {       $('#$name').prop('disabled', ! e.target.checked);      });</script>";
		return $window1;
	}
	public static function createFormDateInput($classname, $name, $caption, $value = null, $isrequired = true, $validationColumnName = null, $placeholder = null, $fieldBlock1 = null)	{
		//$window1 = self::createGeneralFormInput($classname, $name, $caption, $value, $isrequired, "date", $validationColumnName, $placeholder);
		$otherproperties = ""; $title = "";
		if (isset($fieldBlock1['disabled'])) $otherproperties = " disabled";
		if (isset($fieldBlock1['title'])) {
			$ltitle = $fieldBlock1['title'];
			$title = " data-toggle = \"tooltip\" title = \"$ltitle\"";
			$otherproperties .= $title;
		}
		$required = ""; if ($isrequired) $required = "required";
		$fvalue = ""; if (! is_null($value)) {
			try { $value = $value->toGUIDateFormat(); } catch (Exception $e)	{}
			$fvalue = "value = \"$value\"";
		}
		$fplaceholder = ""; if (! is_null($placeholder)) $fplaceholder = "placeholder = \"$placeholder\"";
		if (is_null($validationColumnName)) $validationColumnName = $name;
		$validationRule = Registry::getUIControlValidations($classname, $validationColumnName, "text"); //All Inputs are validated as text , unless otherwise, select control not included
		$randId = __object__::getCodeString(16);
		$window1 =  "<div class=\"form-group row\"><label $title class=\"col-sm-2 col-form-label\"for=\"$name\">$caption</label><div class=\"col-sm-10 input-group date\" id=\"$randId\" data-target-input=\"nearest\"><input $otherproperties class=\"form-control datetimepicker-input\" data-target=\"#$randId\" id=\"$name\" name=\"$name\" type=\"text\" $fvalue $required $fplaceholder $validationRule /><div class=\"input-group-append\" data-target=\"#$randId\" data-toggle=\"datetimepicker\"><div class=\"input-group-text\"><i class=\"fa fa-calendar\"></i></div></div></div></div>";
		$window1 .= "<script>\$('#$randId').datetimepicker({ format: 'L' });</script>";
		/*$window1 .= "<script type=\"text/javascript\">\$('#$randId').datepicker({ format: 'dd/mm/yyyy' });</script>";*/
		return $window1; //Solving updation
	}
	public static function createGeneralFormInput($classname, $name, $caption, $value =null, $isrequired = true, $type = "text", $validationColumnName = null, $placeholder = null, $fieldBlock1 = null)	{
		$otherproperties = ""; $title = "";
		if (isset($fieldBlock1['disabled'])) $otherproperties = " disabled";
		if (isset($fieldBlock1['title'])) {
			$ltitle = $fieldBlock1['title'];
			$title = " data-toggle = \"tooltip\" title = \"$ltitle\"";
			$otherproperties .= $title;
		}
		$required = ""; if ($isrequired) $required = "required";
		$fvalue = ""; if (! is_null($value)) $fvalue = "value = \"$value\"";
		$fplaceholder = ""; if (! is_null($placeholder)) $fplaceholder = "placeholder = \"$placeholder\"";
		if (is_null($validationColumnName)) $validationColumnName = $name;
		$validationRule = Registry::getUIControlValidations($classname, $validationColumnName, "text"); //All Inputs are validated as text , unless otherwise, select control not included
		return "<div class=\"form-group row\"><label $title for=\"$name\" class=\"col-sm-2 col-form-label\">$caption</label><div class=\"col-sm-10\"><input $otherproperties class=\"form-control\" id=\"$name\" name=\"$name\" type=\"$type\" $fvalue $required $fplaceholder $validationRule /></div></div>";
	}
    //01.Flags Management 
    public function setFlagAt($pos)	{
		$powerOfTwo = __object__::getPowerOfTwo($pos);
		if ($powerOfTwo != 0)	{
			$flagValue = intval("".$this->getFlags());
			$flagValue = $flagValue | $powerOfTwo;
			$this->setFlags($flagValue);
		}
	}
	public function resetFlagAt($pos)	{
		$powerOfTwo = __object__::getPowerOfTwo($pos);
		if ($powerOfTwo != 0)	{
			$flagValue = intval("".$this->getFlags());
			$powerOfTwo = self::$__ALL32BITS_SET - $powerOfTwo; //This is a 32bits operation 
			$flagValue = $flagValue & $powerOfTwo;
			$this->setFlags($flagValue);
		}
	}
	public function isFlagSetAt($pos)	{
		$powerOfTwo = __object__::getPowerOfTwo($pos);
		$blnSet = false;
		if ($powerOfTwo != 0)	{
			$flagValue = intval("".$this->getFlags());
			$blnSet = (($flagValue & $powerOfTwo) == $powerOfTwo);
		} 
		return $blnSet;
	}
	//Dealing with updates 
	protected function addToUpdateList($key, $value)	{
		//Make it easier to translate to json format in cols
		$index = sizeof($this->l__update);
		$this->l__update[$index] = array();
		$this->l__update[$index][$key] = $value;
	}
	protected function addToPropertyUpdateList($key, $value)	{
		$this->p_l_update[$key] = $value;
	}
	protected function getUpdateList()	{
		return $this->l__update;
	}
	protected function clearUpdateList()	{
		$this->l__update = null;
		$this->l__update = array();
		$this->p_l_update = null;
		$this->p_l_update = array();
	}
	public function update($rollback = true)	{
		if (sizeof($this->l__update) == 0) throw new Exception("Nothing to update");
		//Check Constraints
		self::evaluateBinaryConstraints($this->p_l_update, $this->getMySystemBinaryConstraints());
		$uArray1 = array();
		$uArray1['query'] = "update";
		$uArray1['table'] = $this->getMyTablename();
		$uArray1['cols'] = $this->l__update;
		$uArray1['where'] = json_decode($this->getIdWhereClause(), true);
		$jresult1 = SQLEngine::execute(json_encode($uArray1), $this->conn, $rollback);
		$jArray1 = json_decode($jresult1, true);
		if (is_null($jArray1)) throw new Exception("Could fetch the status of operation");
		if ($jArray1['code'] !== 0) throw new Exception($jArray1['message']);
		return $this;
	}
	public function delete($rollback = true)	{
		$uArray1 = array();
		$uArray1['query'] = "delete";
		$uArray1['table'] = $this->getMyTablename();
		$uArray1['where'] = json_decode($this->getIdWhereClause(), true);
		$jresult1 = SQLEngine::execute(json_encode($uArray1), $this->conn, $rollback);
		$jArray1 = json_decode($jresult1, true);
		if (is_null($jArray1)) throw new Exception("Could fetch the status of operation");
		if ($jArray1['code'] !== 0) throw new Exception($jArray1['message']);
		return $this;
	}
	public static function getSelectedRecords($conn, $query, $singlerecord = false)	{
		$stmt = $conn->query($query);
		$records = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if (! $records) throw new Exception("[ select ] : Null returned");
		if (sizeof($records) == 0) throw new Exception("[ select ] : Empty records were returned");
		if ($singlerecord && sizeof($records) != 1) throw new Exception("[ select.singlerecord ] : None or Multiple records found");
		return array("column" => $records);
	}
	public static function selectQuery($conn, $classname, $pcolumns, $whereArray = null, $singlerecord = false)	{
		if (is_null($pcolumns)) throw new Exception(["[ select( $classname ) ] : Submitted Empty Column List"]);
		$tablename = Registry::getTablename($classname);
		if (is_null($tablename)) throw new Exception("[ select( $classname ) ] : Could not extract table information");
		$listOfColumns = array();
		foreach ($pcolumns as $pname)	{
			if ($pname == "*") {
				$listOfColumns = array("*");
				break;
			}
			$col = Registry::property2column($classname, $pname);
			if (! is_null($col))	{
				$listOfColumns[sizeof($listOfColumns)] = $col;
			}
		}
		if (sizeof($listOfColumns) == 0) throw new Exception("[ select( $classname ) ] : No column matched");
		$jresult1 = SQLEngine::execute(SimpleQueryBuilder::buildSelect(
			array($tablename),
			$listOfColumns,
			$whereArray
		),$conn);
		if (is_null($jresult1)) throw new Exception("[ select( $classname ) ] : Records could not be returned");
		$jArray1 = json_decode($jresult1, true);
		if (is_null($jArray1['code']) != 0) throw new Exception("[ select( $classname ) ] : ".$jArray1['message']);
		if ($jArray1['count'] == 0) throw new Exception("[ select ( $classname )] : zero records were returned");
		if ($singlerecord && $jArray1['count'] != 1) throw new Exception("[ select( $classname ) ] : Zero or Multiple Records were returned, expected one");
		$tresult = array();
		foreach ($jArray1['rows'] as $row1)	{
			$index = sizeof($tresult);
			$tresult[$index] = array();
			foreach ($row1 as $colname => $value)	{
				$pname = Registry::column2Property($classname, $colname);
				if (is_null($pname)) continue;
				$tresult[$index][$pname] = $value;
			}
		}
		return array("column" => $jArray1['rows'], "property" => $tresult);
	}
	public static function insert($conn, $classname, $pcolumns, $rollback = true, $default_select_empty_value = null)	{
		if (is_null($pcolumns)) throw new Exception("[ insert( $classname ) ] : Submitted Empty Column List");
		$tablename = Registry::getTablename($classname);
		if (is_null($tablename)) throw new Exception("[ insert( $classname ) ] : Could not extract table information");
		$listOfColumns = array();
		foreach ($pcolumns as $pname => $val)	{
			$col  = Registry::property2column($classname, $pname);
			if (! is_null($col)) {
				if (is_null($val) || (! is_null($default_select_empty_value) && ($val == $default_select_empty_value))) continue;
				//We need to make adjustment if necessary
                $refclass = Registry::getReferenceClass($classname, $pname);
                if ($refclass == "DateAndTime") {
                    try { 
                        $val = DateAndTime::convertFromGUIDateFormatToSystemDateAndTimeFormat($val);
                    } catch (Exception $e)    {}
                }
				//You need to do proper validation at this point
				$maxLength = Registry::getMaximumLength($classname, $pname);
				if (! (is_null($maxLength) || ! (strlen($val) > $maxLength))) throw new Exception("[ $pname ($maxLength) ] : Data Length has exceeded the size");
				$regex = Registry::getRegularExpression($classname, $pname);
				if (! (is_null($regex) || preg_match("/".$regex['rule']."/", $val) === 1)) throw new Exception("[ $pname ] : ".$regex['message']);
				$listOfColumns[$col] = $val;
			}
		}
		if (sizeof($listOfColumns) == 0) throw new Exception("[ insert( $classname ) ] : Could not extract Columns Informations");
		//Evaluate constraints
		__data__::evaluateBinaryConstraints($pcolumns, Registry::getSystemBinaryConstraints($classname));
		//Now Saving
		$jresult1 = SQLEngine::execute(SimpleQueryBuilder::buildInsert(
			$tablename, $listOfColumns
		), $conn, $rollback);
		$jArray1 = json_decode($jresult1, true);
		if (is_null($jArray1)) throw new Exception("[ insert( $classname ) ] : Could not extract data from database");
		if ($jArray1['code'] !== 0) throw new Exception($jArray1['message']);
		return $jArray1['id'];
	}
	public static function evaluateBinaryConstraints($payload, $binaryConstraints)	{
		if (is_null($payload)) return true;
		if (is_null($binaryConstraints)) return true;
		foreach ($binaryConstraints as $constraint1)	{
			$lpname = $constraint1['lpname'];
			$rpname = $constraint1['rpname'];
			$op = $constraint1['op'];
			$errorMessage = $constraint1['error-message'];
			$blnNegate = $constraint1['negate'];
			//Extracting values 
			$lvalue = null; if (isset($payload[$lpname])) $lvalue = $payload[$lpname];
			$rvalue = null; if (isset($payload[$rpname])) $rvalue = $payload[$rpname];
			if (is_null($lvalue) || is_null($rvalue)) continue;
			//Calculating boolean
			$blnCalculate = $blnNegate;
			switch ($op)	{
				case "=": $blnCalculate = ($lvalue == $rvalue); break;
				case "==": $blnCalculate = ($lvalue == $rvalue); break;
				case "<": $blnCalculate = ($lvalue < $rvalue); break;
				case "<=": $blnCalculate = ($lvalue <= $rvalue); break;
				case ">": $blnCalculate = ($lvalue > $rvalue); break;
				case ">=": $blnCalculate = ($lvalue >= $rvalue); break;
				case "!": $blnCalculate = ($lvalue != $rvalue); break;
				case "!=": $blnCalculate = ($lvalue !== $rvalue); break;
			}
			if (! ($blnNegate xor $blnCalculate)) throw new Exception($errorMessage);
		}
		return true;
	}
	public abstract function getId();
	public abstract function getId0();
	public abstract function getIdWhereClause();
	public abstract function getId0WhereClause();
    public abstract function setFlags($flags);
	public abstract function getFlags();
	public abstract function getMyClassname();
	public abstract function getMyTablename();	
}
