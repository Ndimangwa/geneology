<?php 
class UITabularView extends UIView {
    public function __construct()   {
        parent::__construct();
    }
    public static function query(
        $conn,
        $sqlquery /*SELECT * FROM _tablename*/, 
        $actionButtons = array(
            array("idColumn" => null,"nameColumn" => null,"caption" => "View","href" => "localhost/","appendId" => false, "link-classes" => null, "link-icons" => null)
        ),
        $dataTranslationArray = null /* arr[dbcolname]['caption'] = "caption" , 
                                                      ['values'][dbval] = displayVal
                                                      ['urlArgsAppend'] = true */ ,
        $doNotDisplayColumns = null,
        $dataMinLength = 3,
        $maximumNumberOfDisplayedRowsPerPage = 64,
        $maximumNumberOfReturnedSearchRecords = 512,
        $formatDataForDisplay = null,
        $containerId = "__ui_tabular_view__ctn__001__"
    )  {
        if (is_null($actionButtons)) $actionButtons = array(
            array("idColumn" => null,"nameColumn" => null, "href" => "localhost/","appendId" => false)
        );
        if (is_null($dataTranslationArray)) $dataTranslationArray = array();
        
        $records = __data__::getSelectedRecords($conn, $sqlquery, false);
        $records = $records['column'];
        $window1 = "<div id=\"$containerId\" class=\"ui-view ui-tabular-view\"><div class=\"bg-primary p-1\"><div class=\"bg-warning p-1\"><div class=\"bg-primary p-1\"><div class=\"bg-white p-1\">";
        /*Begin Your Content Here ---- 0001*/
        //A--Search Control 
        $window1 .= "<div><form><div class=\"input-group mb-3\"><input type=\"search\" class=\"ui-tabular-view-search form-control\" data-min-length=\"$dataMinLength\" placeholder=\"Search\"/><div class=\"input-group-append\"><button type=\"button\" class=\"btn btn-primary ui-tabular-view-btn-search\" data-toggle=\"tooltip\" title=\"Click to Search\">Search</button></div></div></form></div>";
        //B--Tabular
        $window1 .= "<div data-max-rows-per-page=\"$maximumNumberOfDisplayedRowsPerPage\" class=\"table-responsive\"><table class=\"table ui-tabular-view-table\">";
        $count = 0;
        foreach ($records as $record1)  {
            if ($count == 0)    {
                //Develop thead 
                $window1 .= "<thead><tr><th scope=\"col\"></th>";
                foreach ($record1 as $colname => $colval)   {
                    if (! is_null($doNotDisplayColumns) && in_array($colname, $doNotDisplayColumns)) continue;
                    $colcaption = __object__::property2Caption($colname);
                    if (isset($dataTranslationArray[$colname]) && isset($dataTranslationArray[$colname]['caption'])) $colcaption = $dataTranslationArray[$colname]['caption'];
                    $window1 .= "<th>$colcaption</th>";
                }
                $window1 .= "<th></th></tr></thead><tbody>";
            }
            //Dealing with Data
            $sn = $count + 1;
            $window1 .= "<tr><th class=\"data-serial\" scope=\"row\">$sn</th>";
            $urlArgsAppend = null;
            foreach ($record1 as $colname => $colval)   {
                if (isset($dataTranslationArray[$colname]) && isset($dataTranslationArray[$colname]['urlArgsAppend']))  {
                    if (is_null($urlArgsAppend)) $urlArgsAppend = "";
                    $urlArgsAppend .= "&$colname=$colval";
                }
                if (! is_null($doNotDisplayColumns) && in_array($colname, $doNotDisplayColumns)) continue;
                if (isset($dataTranslationArray[$colname]) && isset($dataTranslationArray[$colname]['values']) && isset($dataTranslationArray[$colname]['values'][$colval])) $colval = $dataTranslationArray[$colname]['values'][$colval];
                if (is_callable($formatDataForDisplay)) $colval = $formatDataForDisplay($colname, $colval);
                $window1 .= "<td class=\"data-search\">$colval</td>";
            }
            //Action Buttons
            $window1 .= "<td><span style=\"white-space: nowrap;\">";
            $rowId = null;
            $rowName = null;
            $actionCount = 0;
            foreach ($actionButtons as $actionButton)   {
                //actionButtons
                $actionButtonIdColumn = null; if (isset($actionButton["idColumn"])) $actionButtonIdColumn = $actionButton["idColumn"];
                if (! is_null($actionButtonIdColumn) && isset($record1[$actionButtonIdColumn])) $rowId = $record1[$actionButtonIdColumn];
                $actionButtonNameColumn = null; if (isset($actionButton["nameColumn"])) $actionButtonNameColumn = $actionButton["nameColumn"];
                if (! is_null($actionButtonNameColumn) && isset($record1[$actionButtonNameColumn])) $rowName = $record1[$actionButtonNameColumn];
                $actionButtonCaption = null; if (isset($actionButton["caption"])) $actionButtonCaption = $actionButton["caption"];
                $actionButtonForwardURL = null; if (isset($actionButton["href"])) $actionButtonForwardURL = $actionButton["href"];
                $blnAppendId = false; if (isset($actionButton["appendId"])) $blnAppendId = $actionButton["appendId"];
                $actionButtonTitle = null; if (isset($actionButton["title"])) $actionButtonTitle = $actionButton["title"];
                $actionButtonLinkClasses = ""; if (isset($actionButton["link-classes"])) $actionButtonLinkClasses = $actionButton["link-classes"];
                $actionButtonLinkIcons = null; if (isset($actionButton["link-icons"])) $actionButtonLinkIcons = $actionButton["link-icons"];
                if ($blnAppendId && is_null($rowId)) throw new Exception("Append Id while Column Id not present");
                $linkhref = $actionButtonForwardURL;
                if ($blnAppendId && ! is_null($rowId)) $linkhref .= $rowId;
                if (! is_null($urlArgsAppend)) $linkhref .= $urlArgsAppend;
                $title = null; 
                if (! is_null($actionButtonTitle)) $title = $actionButtonTitle;
                if (! is_null($rowName)) { if (is_null($title)) $title = $rowName; else $title = str_replace("##REPLACE##", $rowName, $title); }
                if (! is_null($title)) $title = " data-toggle=\"tooltip\" title=\"$title\"";
                else $title = "";
                if ($actionCount > 0) {
                    $actionButtonLinkClasses .= " ml-2";
                }
                $actionButtonLinkClasses = "class=\"$actionButtonLinkClasses\"";
                $window1 .= "<a href=\"$linkhref\" $actionButtonLinkClasses $title>";
                if (! is_null($actionButtonLinkIcons)) $window1 .= "<i class=\"$actionButtonLinkIcons\"></i>";
                if (! is_null($actionButtonCaption)) $window1 .= $actionButtonCaption;
                $window1 .= "</a>";
                $actionCount++;
            }
            $window1 .= "</span></td></tr>";
            $count++;
        }
        $window1 .= "</tbody></table></div>";
        
        /*End your Content Here ---- 0001*/
        $window1 .= "</div></div></div></div></div>";
        return $window1;
    }
}
?>