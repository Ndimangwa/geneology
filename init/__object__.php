<?php 
/*
Developed by Ndimangwa Fadhili Ngoya
Developed on 15th April, 2021
Phone: +255 787 101 808 / +255 762 357 596
Email: ndimangwa@gmail.com 

32bits engine
Initialization codes
*/
abstract class __object__ {
    //01.Exception Handling 
    protected function throwMe($message)   {
        self::shootException($message);
    }
    public static function shootException($message)    {
        throw new Exception($message);
    }
    //02. Mathematical Functions 
    public static function getPowerOfTwo($position) {
        $position = intval("".$position);
        if ($position < 0 || $position > 30) return 0;
        return 1 << $position;
    }
    //03. Random Strings
    public static function getRandomMD5Key($salt = null)	{
        $val = rand(10, 999999);
        if (! is_null($salt)) $val = $val.$salt;
		return md5($val);
    }
    public final static function getCodeString($codeLength)	{
        $lk[1] = "A"; $lk[2] = "B"; $lk[3] = "C"; $lk[4] = "D";
       $lk[5] = "E"; $lk[6] = "F"; $lk[7] = "G"; $lk[8] = "H";
       $lk[9] = "I"; $lk[10] = "J"; $lk[11] = "K"; $lk[12] = "L";
       $lk[13] = "M"; $lk[14] = "N"; $lk[15] = "O"; $lk[16] = "P";
       $lk[17] = "Q"; $lk[18] = "R"; $lk[19] = "S"; $lk[20] = "T";
       $lk[21] = "U"; $lk[22] = "V"; $lk[23] = "W"; $lk[24] = "X";
       $lk[25] = "Y" ; $lk[26] = "Z";
       $lk[27] = "0"; $lk[28] = "1"; $lk[29] = "2"; $lk[30] = "3";
       $lk[31] = "4"; $lk[32] = "5"; $lk[33] = "6"; $lk[34] = "7";
       $lk[35] = "8"; $lk[36] = "9";
       $lk[37] = "a"; $lk[38] = "b"; $lk[39] = "c"; $lk[40] = "d";
       $lk[41] = "e"; $lk[42] = "f"; $lk[43] = "g"; $lk[44] = "h";
       $lk[45] = "i"; $lk[46] = "j"; $lk[47] = "k"; $lk[48] = "l";
       $lk[49] = "m"; $lk[50] = "n"; $lk[51] = "o"; $lk[52] = "p";
       $lk[53] = "q"; $lk[54] = "r"; $lk[55] = "s"; $lk[56] = "t";
       $lk[57] = "u"; $lk[58] = "v"; $lk[59] = "w"; $lk[60] = "x";
       $lk[61] = "y"; $lk[62] = "z";
       $codeLength = intval($codeLength);
       $code = "";
       for ($i = 0; $i < $codeLength; $i++)      {
               $code = $code.$lk[rand(1, 62)];
       }
       return $code;
    }
    //04. Strings
    public static function property2Caption($propertyName)	{
		/*
		INPUT nextRegistrationNumber 
		OUTPUT Next Registration Number
		*/
		$propertyArray = preg_split('/(?=[A-Z])/', $propertyName);
		$captionLabel = "";
		foreach ($propertyArray as $val)	{
			if ($val != "")	{
				$captionLabel .= ucfirst($val)." ";
			}
		}
		return $captionLabel;
	}
    public static function summarizeString($string, $summaryLength)	{
		$summaryLength = intval($summaryLength);
		if (strlen($string) > $summaryLength)	{
			//We need to process this string 
			$string = substr($string, 0, $summaryLength);
			$string .= "...";
		}
		return $string;
	}
    public static function fixLength($string1, $length, $pad = "0")  {
        $string1 = "".$string1;
        for ($i = strlen($string1); $i < $length; $i++)  $string1 = $pad.$string1;
        return $string1;
    }
}
