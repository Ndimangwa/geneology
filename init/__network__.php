<?php 
class Network {
	private $ipAddress;
	private $subnetMask;
	private $networkAddress;
	private $broadcastAddress;
	private $numberOfHost;
	public final static function convertASingleBlockToBitString($blockNumber)	{
		//192 
		$blockNumber = intval("".$blockNumber);
		if (($blockNumber < 0) || ($blockNumber > 255)) return null;
		$octet = "";
		while ($blockNumber >= 1)	{
			$octet = ($blockNumber % 2).$octet."";
			$blockNumber = $blockNumber / 2;
		}
		for ($i=strlen($octet); $i < 8; $i++)	$octet = "0".$octet;
		return $octet;
	}
	public final static function convertABitStringBlockToNumber($bitString)	{
		//To speed up prepare a lookup table 
		$positionArray1 = array();
		$positionArray1[0] = 128;
		$positionArray1[1] = 64;
		$positionArray1[2] = 32;
		$positionArray1[3] = 16;
		$positionArray1[4] = 8;
		$positionArray1[5] = 4;
		$positionArray1[6] = 2;
		$positionArray1[7] = 1;
		if (strlen($bitString) != 8) return null;
		$number = 0;
		for ($i = 0; $i < 8; $i++)	{
			if (intval(substr($bitString, $i, 1)) == 1)	$number = $number + $positionArray1[$i];
		}
		return $number;
	}
	public final static function convertIpAddressToBitString($ipAddress)	{
		//Input 192.168.0.1 
		//Output 1100000001000000002000010
		$ipAddressArr = explode(".", $ipAddress);
		if (sizeof($ipAddressArr) != 4) return null;
		$bitString = "";
		foreach ($ipAddressArr as $ip)	{
			$octet = self::convertASingleBlockToBitString($ip);
			if (! is_null($octet))	{
				$bitString = $bitString.$octet;
			} else {
				$bitString = null;
				break;
			}
		}
		return $bitString;
	}
	public final static function convertBitStringToIpAddress($bitString)	{
		if (strlen($bitString) != 32) return null;
		$ipAddress = "";
		$block1 = substr($bitString, 0, 8);
		$ipAddress = self::convertABitStringBlockToNumber($block1);
		$block1 = substr($bitString, 8, 8);
		$ipAddress = $ipAddress.".".self::convertABitStringBlockToNumber($block1);
		$block1 = substr($bitString, 16, 8);
		$ipAddress = $ipAddress.".".self::convertABitStringBlockToNumber($block1);
		$block1 = substr($bitString, 24, 8);
		$ipAddress = $ipAddress.".".self::convertABitStringBlockToNumber($block1);
		return $ipAddress;
	}	
	public final static function isValidIpAddress($ipAddress)	{
		$ipAddressArr = explode(".", $ipAddress);
		if (sizeof($ipAddressArr) != 4) return false;
		$validIp = true;
		foreach ($ipAddressArr as $block1)	{
			$block1 = intval($block1);
			if (! ($block1 >= 0 && $block1 <= 255)) {
				$validIp = false;
				break;
			}
		}
		return $validIp;
	}
	public final static function isValidSubnetMask($netmask)	{
		if (! self::isValidIpAddress($netmask)) return false;
		$bitString = self::convertIpAddressToBitString($netmask);
		if (is_null($bitString)) return false;
		$bitLength = strlen($bitString);
		$validMask = true;
		$state = 0;
		for ($i=0; $i < $bitLength; $i++)	{
			//Fill Transition states only
			if (($state == 0) && (intval(substr($bitString, $i, 1)) == 0)) {
				$state = 1; 
				continue;
			}
			if (($state == 1) && (intval(substr($bitString, $i, 1)) == 1)) {
				$state = 2; //Error State 
				$validMask = false;
				break;
			}
		}
		return $validMask;
	}
	public final static function calculateBroadcastAddress($ipAddress, $subnetMask)	{
		//To be used by Object only, we assume ip and its subnet mask are valid 
		$ipBits = self::convertIpAddressToBitString($ipAddress);
		if (is_null($ipBits)) Object::shootException("A Problem During Conversion");
		$netBits = self::convertIpAddressToBitString($subnetMask);
		if (is_null($netBits)) Object::shootException("A Problem During Conversion");
		$bitLength = strlen($ipBits);
		$networkBits = "";
		for ($i=0; $i < $bitLength; $i++)	{
			$val1 = intval(substr($ipBits, $i, 1));
			$val2 = intval(substr($netBits, $i, 1));
			$bit = 1;
			if ($val2 > $val1) $bit = 0;
			$networkBits .= $bit;
		}
		//convert back to Ip Address 
		return self::convertBitStringToIpAddress($networkBits);
	}
	public final static function calculateNetworkAddress($ipAddress, $subnetMask)	{
		//To be used by Object only, we assume ip and its subnet mask are valid 
		$ipBits = self::convertIpAddressToBitString($ipAddress);
		if (is_null($ipBits)) Object::shootException("A Problem During Conversion");
		$netBits = self::convertIpAddressToBitString($subnetMask);
		if (is_null($netBits)) Object::shootException("A Problem During Conversion");
		$bitLength = strlen($ipBits);
		$networkBits = "";
		for ($i=0; $i < $bitLength; $i++)	{
			$networkBits .= intval(substr($ipBits, $i, 1)) * intval(substr($netBits, $i, 1));
		}
		//convert back to Ip Address 
		return self::convertBitStringToIpAddress($networkBits);
	}
	public function isThisIpAddressBelongToMyNetwork($ipAddress)	{
		if (! Network::isValidIpAddress($ipAddress)) return false;
		$networkAddress = Network::calculateNetworkAddress($ipAddress, $this->subnetMask);
		return (strcmp($this->networkAddress, $networkAddress) == 0);
	}
	public function __construct($ipAddress, $subnetMask)	{
		if (! Network::isValidIpAddress($ipAddress)) Object::shootException("Network, IP Address is not valid");
		if (! Network::isValidSubnetMask($subnetMask)) Object::shootException("Network, Subnet Mask not valid");
		$this->ipAddress = $ipAddress;
		$this->subnetMask = $subnetMask;
		$this->networkAddress = Network::calculateNetworkAddress($ipAddress, $subnetMask);
		$this->broadcastAddress = Network::calculateBroadcastAddress($ipAddress, $subnetMask);
	}
	public function getIpAddress()	{ return $this->ipAddress; }
	public function getSubnetMask()	{ return $this->subnetMask; }
	public function getNetworkAddress()	{ return $this->networkAddress; }
	public function getBroadcastAddress()	{ return $this->broadcastAddress; }
	public function getNumberOfHost()	{ return $this->numberOfHost; }
	public function isAddressInThisNetwork($ipAddress)	{
		return true;
	}
}
?>