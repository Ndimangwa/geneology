<div class="container data-container mt-2 mb-2">
    <div class="row">
        <!-- <div class="col-md-6 d-none d-md-block text-primary"><div class="mb-2 display-2 p-2" style="height: 100%; position: relative;"><span style="position: relative; top: 25%;"><i><?= $profile1->getProfileName() ?></i></span></div></div>
-->
        <div class="col-md-10 offset-md-1 mx-auto">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    CREATE A NEW EVENT
                </div>
                <div class="card-body">
                    <?php
                    $conn = null;
                    $nextPage = $thispage . "?page=event";
                    $rollback = false;
                    try {
                        $conn = new PDO("mysql:host=$host;dbname=$dbname", $config1->getUsername(), $config1->getPassword());
                        if (isset($_POST['submit-data'])) {
                            $a = $_POST['person'] == Constant::$default_select_empty_value;
                            $b = trim($_POST['ageOfAPersonDuringTheEvent']) == "";
                            $c = $_POST['event'] == Constant::$default_select_empty_value;
                            $d = trim($_POST['yearsElapsedAfterTheEvent']) == "";
                            if (($a xor $b) || ($c xor $d) || ($b && $d)) throw new Exception("Combination of Person/Event and with the years is not allowed");
                            $colArray1 = $_POST;
                            $colArray1['timeOfCreation'] = $systemTime1->getDateAndTimeString();
                            $colArray1['timeOfUpdation'] = $systemTime1->getDateAndTimeString();
                            $conn->beginTransaction();
                            $rollback = true;
                            $event1 = new Event("Delta Init", __data__::insert($conn, "Event", $colArray1, false, Constant::$default_select_empty_value), $conn);
                            $person1 = $event1->getPerson();
                            $refEvent1 = $event1->getEvent();
                            if (! is_null($person1))    {
                                $event1->setEventYear($person1->getYearOfBirth() + $event1->getAgeOfAPersonDuringTheEvent());
                            } else if (! is_null($refEvent1))   {
                                $event1->setEventYear($refEvent1->getEventYear() + $event1->getYearsElapsedAfterTheEvent());
                            }
                            $event1->update(false);
                            $conn->commit();
                            $rollback = false;
                            $message = "[ ".$event1->getEventName()." ] created";
                            SystemLogs::addLog2($conn, $systemTime1->getDateAndTimeString(), $login1->getLoginName(), "event_create", $message);
                            echo UICardView::getSuccesfulReportCard($event1->getEventName(), $message." successful");
                        } else {
                            echo __data__::createDataCaptureForm($nextPage, "Event", array(
                                array('pname' => 'eventName', 'caption' => 'Event Name', 'required' => true, 'placeholder' => 'Flood During Noah Time'),
                                array('pname' => 'person', 'caption' => 'Reference Person', 'required' => false, 'title' => 'A person whom we can refer years'),
                                array('pname' => 'ageOfAPersonDuringTheEvent', 'caption' => 'Age of The Person During Event', 'required' => false, 'placeholder' => '9'),
                                array('pname' => 'event', 'caption' => 'Reference Event', 'required' => false),
                                array('pname' => 'yearsElapsedAfterTheEvent', 'caption' => 'Years Elapsed After Event', 'required' => false, 'placeholder' => '3'),
                                array('pname' => 'comments', 'caption' => 'Comments', 'required' => false, 'placeholder' => 'Option Comments')
                            ), "Add A New Event", "create", $conn, 0, array(
                                'page' => $page,
                                'submit-data' => 1
                            ), null, null, 'default-class', $thispage, true);
                        }
                    } catch (Exception $e) {
                        if ($rollback) $conn->rollBack();
                        echo __data__::showDangerAlert($e->getMessage());
                    }
                    $conn = null;
                    ?>
                </div>
                <div class="card-footer">
                    <div class="text-center">
                        <i><a href="<?= $thispage ?>?page=<?= $page ?>" class="card-link mr-2">Add Another Event</a></i>
                        <i><a href="<?= $nextPage ?>" class="card-link">Back to Event</a></i><br />
                        <span class="text-muted"><i>Rule: event_create</i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>