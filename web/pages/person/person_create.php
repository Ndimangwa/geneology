<div class="container data-container mt-2 mb-2">
    <div class="row">
        <!-- <div class="col-md-6 d-none d-md-block text-primary"><div class="mb-2 display-2 p-2" style="height: 100%; position: relative;"><span style="position: relative; top: 25%;"><i><?= $profile1->getProfileName() ?></i></span></div></div>
-->
        <div class="col-md-10 offset-md-1 mx-auto">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    CREATE A NEW PERSON
                </div>
                <div class="card-body">
                    <?php
                    $conn = null;
                    $nextPage = $thispage . "?page=person";
                    $rollback = false;
                    try {
                        $conn = new PDO("mysql:host=$host;dbname=$dbname", $config1->getUsername(), $config1->getPassword());
                        if (isset($_POST['submit-data'])) {
                            $a = $_POST['father'] == Constant::$default_select_empty_value;
                            $b = trim($_POST['ageOfFatherDuringBirth']) == "";
                            $c = $_POST['ageOfFatherDuringBirth'] > 0;
                            $d = $_POST['mother'] == Constant::$default_select_empty_value;
                            $e = trim($_POST['ageOfMotherDuringBirth']) == "";
                            $f = $_POST['ageOfMotherDuringBirth'] > 0;
                            $g = $_POST['person'] == Constant::$default_select_empty_value;
                            $h = trim($_POST['ageOfAPersonDuringBirth']) == "";
                            if (
                                ($a xor $b) || (! $a && ! $c)
                                || ($d xor $e) || (! $d && ! $f)
                                || ($g xor $h) || ($b && $e && $h)
                            ) throw new Exception("Combination of arguments were not properly");
                            $colArray1 = $_POST;
                            $colArray1['timeOfCreation'] = $systemTime1->getDateAndTimeString();
                            $colArray1['timeOfUpdation'] = $systemTime1->getDateAndTimeString();
                            $conn->beginTransaction();
                            $rollback = true;
                            $person1 = new Person("Data", __data__::insert($conn, "Person", $colArray1, false, Constant::$default_select_empty_value), $conn);
                            $father1 = $person1->getFather();
                            $mother1 = $person1->getMother();
                            if (! is_null($father1) && ($person1->getAgeOfFatherDuringBirth() != 0))    {
                                $person1->setYearOfBirth($father1->getYearOfBirth() + $person1->getAgeOfFatherDuringBirth());
                            } else if (! is_null($mother1) && ($person1->getAgeOfMotherDuringBirth() != 0)) {
                                $person1->setYearOfBirth($mother1->getYearOfBirth() + $person1->getAgeOfMotherDuringBirth());
                            }
                            if ($person1->getTotalYearsLived() != 0)    {
                                $person1->setYearOfDeath($person1->getYearOfBirth() + $person1->getTotalYearsLived());
                            }
                            $person1->update(false);
                            $conn->commit();
                            $rollback = false;
                            $message = "[ ".$person1->getFullName()." ] created";
                            SystemLogs::addLog2($conn, $systemTime1->getDateAndTimeString(), $login1->getLoginName(), "person_create", $message);
                            echo UICardView::getSuccesfulReportCard($person1->getFullName(), $message." successful");
                        } else {
                            echo __data__::createDataCaptureForm($nextPage, "Person", array(
                                array('pname' => 'personName', 'caption' => 'Name', 'required' => true, 'placeholder' => 'Name of the Person'),
                                array('pname' => 'sex', 'caption' => 'Sex', 'required' => true, 'placeholder' => 'Sex'),
                                array('pname' => 'father', 'caption' => 'Father', 'required' => false, 'placeholder' => 'Father', 'filter' => array('sex' => array(Sex::$__MALE))),
                                array('pname' => 'ageOfFatherDuringBirth', 'caption' => 'Father\'s Age During Birth', 'required' => false, 'placeholder' => '36'),
                                array('pname' => 'mother', 'caption' => 'Mother', 'required' => false, 'placeholder' => 'Mother', 'filter' => array('sex' => array(Sex::$__FEMALE))),
                                array('pname' => 'ageOfMotherDuringBirth', 'caption' => 'Mother\'s Age During Birth', 'required' => false, 'placeholder' => '32'),
                                array('pname' => 'person', 'caption' => 'Person', 'required' => false, 'title' => 'If Father Age or Mother Age not known, we may rely this person to calculate age'),
                                array('pname' => 'ageOfPersonDuringBirth', 'caption' => 'Age of a Person During Birth', 'required' => false, 'placeholder' => '17'),
                                array('pname' => 'event', 'caption' => 'Event', 'required' => false, 'title' => 'If no any reference person, we can rely on a known event'),
                                array('pname' => 'yearsElapsedAfterTheEvent', 'caption' => 'Event Elapsed Years', 'required' => false, 'placeholder' => '3'),
                                array('pname' => 'totalYearsLived', 'caption' => 'Total Years Lived', 'required' => false, 'placeholder' => '0'),
                                array('pname' => 'comments', 'caption' => 'Comments', 'required' => false, 'placeholder' => 'Option Comments')
                            ), "Add A New Person", "create", $conn, 0, array(
                                'page' => $page,
                                'submit-data' => 1
                            ), null, null, 'default-class', $thispage, true);
                        }
                    } catch (Exception $e) {
                        if ($rollback) $conn->rollBack();
                        echo __data__::showDangerAlert($e->getMessage());
                    }
                    $conn = null;
                    ?>
                </div>
                <div class="card-footer">
                    <div class="text-center">
                        <i><a href="<?= $thispage ?>?page=<?= $page ?>" class="card-link mr-2">Add Another Person</a></i>
                        <i><a href="<?= $nextPage ?>" class="card-link">Back to Person</a></i><br />
                        <span class="text-muted"><i>Rule: person_create</i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>