<div class="container data-container mt-2 mb-2">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    PERSON MANAGEMENT
                </div>
                <div class="card-body">
<?=
    Person::getASearchUI($thispage, array('personName', 'sex', 'father', 'mother'));
?>
                    <div class="mt-2">
                        <a class="btn btn-primary btn-block" href="<?= $thispage ?>?page=person_search&geneology=1">View Geneology Table</a>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="text-center text-md-right mb-2">
                        <a href="<?= $thispage ?>?page=person_create" data-toggle="tooltip" title="Add A New Record" class="btn btn-primary mr-2 add-record">Add New
                            Record</a>
                    </div>
                    <div class="text-center">
                        <span class="text-muted"><i>Rule: person</i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>