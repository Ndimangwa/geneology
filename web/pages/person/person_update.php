<div class="container data-container mt-2 mb-2">
    <div class="row">
        <!--<div class="col-md-6 d-none d-md-block text-primary"><div class="mb-2 display-2 p-2" style="height: 100%; position: relative;"><span style="position: relative; top: 25%;"><i><?= $profile1->getProfileName() ?></i></span></div></div>
-->
        <div class="col-md-10 offset-md-1 mx-auto">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    UPDATING A PERSON
                </div>
                <div class="card-body">
                    <?php
                    $conn = null;
                    $nextPage = $thispage . "?page=person";
                    $rollback = false;
                    try {
                        $conn = new PDO("mysql:host=$host;dbname=$dbname", $config1->getUsername(), $config1->getPassword());
                        $person1 = new Person("Delta", $_REQUEST['id'], $conn);
                        if (isset($_POST['submit-data'])) {
                            if ($_POST['efilter'] != $person1->getExtraFilter()) throw new Exception("This page can be submitted only once");
                            $colArray1 = array();
                            $conn->beginTransaction();
                            $rollback = true;
                            $person1->addToUpdateList("timeOfUpdation", $systemTime1->getDateAndTimeString());
                            foreach ($_POST as $pname => $value) {
                                $colname = Person::property2Column($pname);
                                $value = trim($value);
                                if (is_null($value) || $value == "" || $value == Constant::$default_select_empty_value) continue;
                                if (is_null($colname)) continue;
                                if ($pname == "ageOfFatherDuringBirth") {
                                    $person1->setAgeOfFatherDuringBirth($value);
                                } else if ($pname == "ageOfMotherDuringBirth") {
                                    $person1->setAgeOfMotherDuringBirth($value);
                                } else if ($pname == "totalYearsLived") {
                                    $person1->setTotalYearsLived($value);
                                } else {
                                    $person1->addToUpdateList($colname, $value);
                                }
                            }
                            $father1 = $person1->getFather();
                            $mother1 = $person1->getMother();
                            if (!is_null($father1) && ($person1->getAgeOfFatherDuringBirth() != 0)) {
                                $person1->setYearOfBirth($father1->getYearOfBirth() + $person1->getAgeOfFatherDuringBirth());
                            } else if (!is_null($mother1) && ($person1->getAgeOfMotherDuringBirth() != 0)) {
                                $person1->setYearOfBirth($mother1->getYearOfBirth() + $person1->getAgeOfMotherDuringBirth());
                            }
                            if ($person1->getTotalYearsLived() != 0) {
                                $person1->setYearOfDeath($person1->getYearOfBirth() + $person1->getTotalYearsLived());
                            }
                            $person1->setExtraFilter(__object__::getCodeString(32));
                            $person1->update(false);
                            $conn->commit();
                            $rollback = false;
                            $message = "[ " . $person1->getFullName() . " ] updated";
                            SystemLogs::addLog2($conn, $systemTime1->getDateAndTimeString(), $login1->getLoginName(), "person_update", $message);
                            echo UICardView::getSuccesfulReportCard($person1->getFullName(), $message . " successful");
                        } else {
                            $person1->setExtraFilter(__object__::getCodeString(32))->update(true);
                            echo __data__::createDataCaptureForm($nextPage, "Person", array(
                                array('pname' => 'personName', 'caption' => 'Name', 'required' => true, 'placeholder' => 'Name of the Person'),
                                array('pname' => 'sex', 'caption' => 'Sex', 'required' => true, 'placeholder' => 'Sex'),
                                array('pname' => 'father', 'caption' => 'Father', 'required' => false, 'placeholder' => 'Father', 'filter' => array('sex' => array(Sex::$__MALE))),
                                array('pname' => 'ageOfFatherDuringBirth', 'caption' => 'Father\'s Age During Birth', 'required' => false, 'placeholder' => '36'),
                                array('pname' => 'mother', 'caption' => 'Mother', 'required' => false, 'placeholder' => 'Mother', 'filter' => array('sex' => array(Sex::$__FEMALE))),
                                array('pname' => 'ageOfMotherDuringBirth', 'caption' => 'Mother\'s Age During Birth', 'required' => false, 'placeholder' => '32'),
                                array('pname' => 'person', 'caption' => 'Person', 'required' => false, 'title' => 'If Father Age or Mother Age not known, we may rely this person to calculate age'),
                                array('pname' => 'ageOfPersonDuringBirth', 'caption' => 'Age of a Person During Birth', 'required' => false, 'placeholder' => '17'),
                                array('pname' => 'event', 'caption' => 'Event', 'required' => false, 'title' => 'If no any reference person, we can rely on a known event'),
                                array('pname' => 'yearsElapsedAfterTheEvent', 'caption' => 'Event Elapsed Years', 'required' => false, 'placeholder' => '3'),
                                array('pname' => 'totalYearsLived', 'caption' => 'Total Years Lived', 'required' => false, 'placeholder' => '0'),
                                array('pname' => 'comments', 'caption' => 'Comments', 'required' => false, 'placeholder' => 'Option Comments')
                            ), "Update a Person", "update", $conn, $_GET['id'], array(
                                'page' => $page,
                                'id' => $_GET['id'],
                                'submit-data' => 1,
                                'efilter' => $person1->getExtraFilter()
                            ), null, null, 'default-class', $thispage, true);
                        }
                    } catch (Exception $e) {
                        if ($rollback) $conn->rollBack();
                        echo __data__::showDangerAlert($e->getMessage());
                    }
                    $conn = null;
                    ?>
                </div>
                <div class="card-footer">
                    <div class="text-center">
                        <i><a href="<?= $nextPage ?>" class="card-link">Back to Person</a></i><br />
                        <span class="text-muted"><i>Rule: person_update</i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>