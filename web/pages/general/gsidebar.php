<!-- Main Sidebar Container -->
<?php
$pageprefix = $thispage; //We will update /system/
?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="../images/mbwambo.png" alt="<?= $profile1->getProfileName() ?>" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light"><?= $profile1->getProfileName() ?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <!--<div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="../cdns/AdminLTE-3.1.0/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Alexander Pierce</a>
            </div>
        </div> -->

        <!-- SidebarSearch Form -->
        <!--<div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>-->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="<?= $pageprefix ?>" class="nav-link active">
                        <i class="nav-icon fas fa-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <?php
                if ($login1->isRoot()) {
                ?>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon"></i>
                            <p>User Accounts
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= $pageprefix ?>?page=group" class="nav-link">
                                    <i class="nav-icon fas fa-circle"></i>
                                    <p>User Groups</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= $pageprefix ?>?page=jobtitle" class="nav-link">
                                    <i class="nav-icon fas fa-circle"></i>
                                    <p>Job Title</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= $pageprefix ?>?page=systemuser" class="nav-link">
                                    <i class="nav-icon fas fa-user"></i>
                                    <p>System User</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php
                }
                if (Authorize::isAllowable($config1, "person", "normal", "donotsetlog", null, null)) {
                ?>
                    <li class="nav-item">
                        <a href="<?= $pageprefix ?>?page=person" class="nav-link">
                            <i class="nav-icon"></i>
                            <p>Person</p>
                        </a>
                    </li>
                <?php
                }
                if (Authorize::isAllowable($config1, "event", "normal", "donotsetlog", null, null)) {
                ?>
                    <li class="nav-item">
                        <a href="<?= $pageprefix ?>?page=event" class="nav-link">
                            <i class="nav-icon"></i>
                            <p>Event</p>
                        </a>
                    </li>
                <?php
                }
                if (Authorize::isAllowable($config1, "menu_mysystem", "normal", "donotsetlog", null, null)) {
                ?>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon"></i>
                            <p>My System
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <?php
                            if (Authorize::isAllowable($config1, "update_my_login", "normal", "donotsetlog", null, null)) {
                            ?>
                                <li class="nav-item">
                                    <a href="<?= $pageprefix ?>?page=update_my_login" class="nav-link">
                                        <i class="nav-icon fas fa-user-circle"></i>
                                        <p>My Profile</p>
                                    </a>
                                </li>
                            <?php
                            }
                            if ($login1->isRoot()) {
                            ?>
                                <li class="nav-item">
                                    <a href="<?= $pageprefix ?>?page=profile_update" class="nav-link">
                                        <i class="nav-icon fas fa-cog"></i>
                                        <p>System Settings</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= $pageprefix ?>?page=notificationmanager" class="nav-link">
                                        <i class="nav-icon fas fa-cog"></i>
                                        <p>Notification Manager</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= $pageprefix ?>?page=lastresortdonotcare" class="nav-link">
                                        <i class="nav-icon fas fa-lock"></i>
                                        <p>Last Resort</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= $pageprefix ?>?page=systemlogs" class="nav-link">
                                        <i class="nav-icon"></i>
                                        <p>System Logs</p>
                                    </a>
                                </li>
                            <?php
                            }
                            ?>
                        </ul>
                    </li>
                <?php
                }
                ?>
                <li class="nav-item">
                    <a id="logoutButton" href="#" class="nav-link">
                        <i class="nav-icon"></i>
                        <p>Logout</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
<!--End Main Sidebar Container-->
<script>
    (function($) {
        $(function() {
            $('a.nav-link').on('click', function(e) {
                var $button1 = $(this);
                var $navTree1 = $button1.closest('ul.nav-sidebar');
                $navTree1.find('a.nav-link.active').removeClass('active');
                $button1.addClass('active');
                var $treeView1 = $button1.closest('ul.nav-treeview');
                if ($treeView1.length) {
                    var $link1 = $treeView1.closest('li.nav-item');
                    if ($link1.length) {
                        $link1 = $link1.find('a.nav-link').first();
                        if ($link1.length) {
                            $link1.addClass('active');
                        }
                    }
                }
            });
        });
    })(jQuery);
</script>