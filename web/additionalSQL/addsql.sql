DROP TABLE IF EXISTS _contextManager;
CREATE TABLE _contextManager (
    defaultXValue tinyint not null default 1,
    primary key (defaultXValue)
)engine=innoDB;
INSERT INTO _contextManager(defaultXValue) VALUES(1);
DROP TABLE IF EXISTS _contextLookup;
CREATE TABLE _contextLookup(
	contextId INT UNSIGNED NOT NULL AUTO_INCREMENT,
	symbol CHAR(1) NOT NULL,
	_value CHAR(4) NOT NULL UNIQUE,
	extraFilter VARCHAR(32),
	extraInformation VARCHAR(64),
	flags INT DEFAULT 0,
	PRIMARY KEY(contextId)) engine=innoDB;
INSERT INTO _contextLookup (symbol,_value) VALUES ('A','0000'), ('B','0001'), ('C','0002'), ('D','0010'), ('E','0011'), ('F','0012'), ('G','0020'), ('H','0021'), ('I','0022'), ('J','0100'), ('K','0101'), ('L','0102'), ('M','0110'), ('N','0111'), ('O','0112'), ('P','0120'), ('Q','0121'), ('R','0122'), ('S','0200'), ('T','0201'), ('U','0202'), ('V','0210'), ('W','0211'), ('X','0212'), ('Y','0220'), ('Z','0221'), ('a','0222'), ('b','1000'), ('c','1001'), ('d','1002'), ('e','1010'), ('f','1011'), ('g','1012'), ('h','1020'), ('i','1021'), ('j','1022'), ('k','1100'), ('l','1101'), ('m','1102'), ('n','1110'), ('o','1111'), ('p','1112'), ('q','1120'), ('r','1121'), ('s','1122'), ('t','1200'), ('u','1201'), ('v','1202'), ('w','1210'), ('x','1211'), ('y','1212'), ('z','1220'), ('0','1221'), ('1','1222'), ('2','2000'), ('3','2001'), ('4','2002'), ('5','2010'), ('6','2011'), ('7','2012'), ('8','2020'), ('9','2021'), ('!','2022'), ('@','2100'), ('#','2101'), ('%','2102'), ('&','2110'), ('(','2111'), (')','2112'), ('{','2120'), ('}','2121'), ('[','2122'), (']','2200'), ('<','2201'), ('>','2202'), ('?','2210'), ('+','2211'), ('*','2212'), ('|','2220'), (':','2221'), (';','2222');
DROP TABLE IF EXISTS _contextPosition;
CREATE TABLE _contextPosition(
	cId INT UNSIGNED NOT NULL AUTO_INCREMENT,
	cName VARCHAR(56) NOT NULL UNIQUE,
	cPosition INT NOT NULL UNIQUE,
	caption VARCHAR(96) NOT NULL,
	extraFilter VARCHAR(32),
	extraInformation VARCHAR(64),
	flags INT DEFAULT 0,
	PRIMARY KEY(cId)) engine=innoDB;
	INSERT INTO _contextPosition (cName, cPosition, caption) VALUES 
			('login', 1, 'User Accounts Management'), 
			('login_create', 2, 'Create User Account'),
			('login_read', 3, 'View User Account'), 
			('login_update', 4, 'Update User Account'), 
			('login_delete', 5, 'Delete User Account'),
			('login_search', 6, 'Search User Accounts'),
			('profile', 7, 'General System Settings'),
			('profile_update', 8, 'Update System Settings'), 
			('profile_read', 9, 'View System Settings'),
			('group', 10, 'General Group Operation'),
			('group_create',11, 'Create a User Group'),
			('group_read', 12, 'View a User Group'),
			('group_update', 13, 'Update a User Group'),
			('group_delete', 14, 'Delete a User Group'),
			('group_search', 15, 'Search User Groups'),
			('jobtitle', 16, 'General Jobtitle Operations'),
			('jobtitle_create', 17, 'Create a User Jobtitle'),
			('jobtitle_read', 18, 'View a User Jobtitle'),
			('jobtitle_update', 19, 'Update a User Jobtitle'),
			('jobtitle_delete', 20, 'Delete a User Jobtitle'),
			('jobtitle_search', 21, 'Search User Jobtitles'),
			('menu_mysystem', 22, 'My System Menu'),
			('menu_users', 23, 'System Users'),
			('update_my_login', 24, 'Update My User Accounts'),
			('firewall', 25, 'Firewall/Authorization Management'),
			('firewall_update', 26, 'Update Firewall'),
			('firewall_read', 27, 'View Firewalls'),
			('system_lastresort_donotcare', 28, 'System Decision on Do not care'),
			('systemlogs', 29, 'General System Logs Management'),
			('systemlogs_search', 30, 'Search System Logs'),
			('notificationmanager', 31, 'Notification Manager Services'),
			('notificationmanager_create', 32, 'Create a New Notification Manager'),
			('notificationmanager_read', 33, 'Details/View a Notification Manager'),
			('notificationmanager_update', 34, 'Update a Notification Manager'),
			('notificationmanager_delete', 35, 'Delete a Notification Manager'),
			('notificationmanager_search', 36, 'Search a Notification Manager'),
			('systemuser', 37, 'General System User Management'),
			('systemuser_create', 38, 'Create a System User'),
			('systemuser_read', 39, 'Details/View of a System User'),
			('systemuser_update', 40, 'Update a System User'),
			('systemuser_delete', 41, 'Delete a System User'),
			('systemuser_search', 42, 'Search a System User'),
			('person', 43, 'General Person Management'),
			('person_create', 44, 'Create a Person'),
			('person_read', 45, 'Details/View of a Person'),
			('person_update', 46, 'Update a Person'),
			('person_delete', 47, 'Delete a Person'),
			('person_search', 48, 'Search a Person'),
			('event', 49, 'General Event Management'),
			('event_create', 50, 'Create an Event'),
			('event_read', 51, 'Details/View of an Event'),
			('event_update', 52, 'Update an Event'),
			('event_delete', 53, 'Delete an Event'),
			('event_search', 54, 'Search an Event');
DROP TABLE IF EXISTS _navigationMenu;
CREATE TABLE _navigationMenu(
	menuId INT UNSIGNED NOT NULL AUTO_INCREMENT,
	menuName VARCHAR(255) NOT NULL,
	pageToGo VARCHAR(255),
	seqno INT DEFAULT 0,
	pId INT UNSIGNED DEFAULT NULL,
	cName VARCHAR(56),
	flags INT DEFAULT 0,
	FOREIGN KEY(pId) REFERENCES _navigationMenu(menuId),
	PRIMARY KEY(menuId)) engine=innoDB;
INSERT INTO _navigationMenu (menuId,menuName,pageToGo,seqno,pId,cName) VALUES (1,'Staff/User Management','',0,NULL,'menu_users'), (2,'Patient Management','',0,NULL,'menu_patients'), (3,'Financial Management','',0,NULL,'finance'), (4,'Patient Flow Management','',0,NULL,'patient_flow'), (5,'Laboratory Management','',0,NULL,'laboratory'), (6,'Admission Management','',0,NULL,'admission'), (7,'Pharmacy Management','',0,NULL,'pharmacy'), (8,'Ward & Accomodation Management','',0,NULL,'accomodation'), (9,'User Accounts Management','page=login',0,1,'login'), (10,'Job Title Management','page=jobtitle',0,1,'jobtitle'), (11,'Group Management','page=group',0,1,'group');

DROP TABLE IF EXISTS _systemlogs;
CREATE TABLE _systemlogs(
	logId INT UNSIGNED NOT NULL AUTO_INCREMENT,
	logDate VARCHAR(19) NOT NULL DEFAULT '0000:00:00:00:00:00',
	username VARCHAR(56) NOT NULL,
	contextPosition INT UNSIGNED NOT NULL,
	opname VARCHAR(56) NOT NULL,
	target VARCHAR(96) NOT NULL,
	flags INT DEFAULT 0,
	FOREIGN KEY(contextPosition) REFERENCES _contextPosition(cId),
	PRIMARY KEY(logId)) engine=innoDB;

